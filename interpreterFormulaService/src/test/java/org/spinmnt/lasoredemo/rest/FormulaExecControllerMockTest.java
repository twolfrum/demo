package org.spinmnt.lasoredemo.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.spinmnt.lasoredemo.rest.FormulaExecController;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(locations = "classpath:formularest-servlet.xml")
public class FormulaExecControllerMockTest  {
	
	private MockMvc restMockMvc;
	private HashMap<String, Object> formulaInput;
	
	private static final MediaType APPLICATION_JSON_UTF8 = new MediaType(
            MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), StandardCharsets.UTF_8);
	
	private static final ObjectMapper mapper = new ObjectMapper();
	
	private static Map<String, Object> parseResult(MvcResult result) {
		try {
			String contentAsString = result.getResponse().getContentAsString();
			return mapper.readValue(contentAsString, Map.class);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Before
    public void setup() {
		final FormulaExecController formulaExec = new FormulaExecController();
		this.restMockMvc = MockMvcBuilders.standaloneSetup(formulaExec).build();
		formulaInput = new HashMap<String, Object>();
	}
	
	@Test
	public void execFormulaOne() throws Exception {
		formulaInput.put("The Number On Top", 53);
		formulaInput.put("The Number On Bottom", 17);
		
		MvcResult mvcResult = restMockMvc.perform(post("/formula/exec/FormulaOne")
	            .contentType(APPLICATION_JSON_UTF8)
	            .content(mapper.writeValueAsBytes(formulaInput)))
	            .andExpect(status().isOk())
	            .andReturn();
		
		Map<String, Object> result = parseResult(mvcResult);
		assert(result.get("TheResult").toString().equals("3.12"));
	}
	
	@Test
	public void execFormulaTwo() throws Exception {
		formulaInput.put("Fun Type", "Sailing");
		formulaInput.put("DateOfFun", "2020-06-12");
		formulaInput.put("Fun Adjustment", 6.34);
		formulaInput.put("NormalizeCode", "N2");
		formulaInput.put("Expiration", "2020-08-15");
		formulaInput.put("MaxFun", 99.99);
		formulaInput.put("ZMultiplier", 8);
		
		MvcResult mvcResult = restMockMvc.perform(post("/formula/exec/FormulaTwo")
	            .contentType(APPLICATION_JSON_UTF8)
	            .content(mapper.writeValueAsBytes(formulaInput)))
	            .andExpect(status().isOk())
	            .andReturn();
		
		Map<String, Object> result = parseResult(mvcResult);
		assert(result.get("TheResult").toString().equals("50.385"));
	}
	
	@Test
	public void execFormulaThree() throws Exception {
		formulaInput.put("Apples", 21);
		formulaInput.put("Peaches", 37.3);
		formulaInput.put("Pears", 6.45);
		
		MvcResult mvcResult = restMockMvc.perform(post("/formula/exec/FormulaThree")
	            .contentType(APPLICATION_JSON_UTF8)
	            .content(mapper.writeValueAsBytes(formulaInput)))
	            .andExpect(status().isOk())
	            .andReturn();
		
		Map<String, Object> result = parseResult(mvcResult);
		assert(result.get("AllTheFruits").toString().equals("64.75"));
	}
	
	@Test
	public void execFormulaXRed() throws Exception {
		formulaInput.put("Apples", 21);
		formulaInput.put("Peaches", 37.3);
		formulaInput.put("Pears", 6.45);
		formulaInput.put("FruitMult", 11);
		formulaInput.put("Red", "YES");
		formulaInput.put("Green", 56);
		formulaInput.put("MaxGreen", 70);
		formulaInput.put("MinGreen", 13);
		formulaInput.put("Tickler", 2.174);
		
		MvcResult mvcResult = restMockMvc.perform(post("/formula/exec/FormulaX")
	            .contentType(APPLICATION_JSON_UTF8)
	            .content(mapper.writeValueAsBytes(formulaInput)))
	            .andExpect(status().isOk())
	            .andReturn();
		
		Map<String, Object> result = parseResult(mvcResult);
		assert(result.get("TheMotherOfAllResults").toString().equals("714.42"));
	}
	
	@Test
	public void execFormulaXGreen() throws Exception {
		formulaInput.put("Apples", 21);
		formulaInput.put("Peaches", 37.3);
		formulaInput.put("Pears", 6.45);
		formulaInput.put("FruitMult", 11);
		formulaInput.put("Red", "NO");
		formulaInput.put("Green", 56);
		formulaInput.put("MaxGreen", 81);
		formulaInput.put("MinGreen", 13);
		formulaInput.put("Tickler", 2.174);
		
		MvcResult mvcResult = restMockMvc.perform(post("/formula/exec/FormulaX")
	            .contentType(APPLICATION_JSON_UTF8)
	            .content(mapper.writeValueAsBytes(formulaInput)))
	            .andExpect(status().isOk())
	            .andReturn();
		
		Map<String, Object> result = parseResult(mvcResult);
		assert(result.get("TheMotherOfAllResults").toString().equals("58.17"));
	}
	
	@Test
	public void execFormulaXDefault() throws Exception {
		formulaInput.put("Apples", 21);
		formulaInput.put("Peaches", 37.3);
		formulaInput.put("Pears", 6.45);
		formulaInput.put("FruitMult", 11);
		formulaInput.put("Red", "NO");
		formulaInput.put("Green", 9);
		formulaInput.put("MaxGreen", 81);
		formulaInput.put("MinGreen", 13);
		formulaInput.put("Tickler", 2.174);
		
		MvcResult mvcResult = restMockMvc.perform(post("/formula/exec/FormulaX")
	            .contentType(APPLICATION_JSON_UTF8)
	            .content(mapper.writeValueAsBytes(formulaInput)))
	            .andExpect(status().isOk())
	            .andReturn();
		
		Map<String, Object> result = parseResult(mvcResult);
		assert(result.get("TheMotherOfAllResults").toString().equals("70.17"));
	}
	
	@Test
	public void execFormulaCompoundBoolCondition1() throws Exception {
		formulaInput.put("Red", "YES");
		formulaInput.put("Secret", "MUBB");
		formulaInput.put("Green", 100);
		formulaInput.put("MaxGreen", 81);
		formulaInput.put("FunMeter", 9.56);
		formulaInput.put("MaxFun", 12.35);
		formulaInput.put("ZMultiplier", 14);
		
		MvcResult mvcResult = restMockMvc.perform(post("/formula/exec/FormulaCompoundBool")
	            .contentType(APPLICATION_JSON_UTF8)
	            .content(mapper.writeValueAsBytes(formulaInput)))
	            .andExpect(status().isOk())
	            .andReturn();
		
		Map<String, Object> result = parseResult(mvcResult);
		assert(result.get("TheResult").toString().equals("9.56"));
		assert(result.get("Condition1") != null);
	}
	
	@Test
	public void execFormulaCompoundBoolCondition2() throws Exception {
		formulaInput.put("Red", "NO");
		formulaInput.put("Secret", "ZERO");
		formulaInput.put("Green", 100);
		formulaInput.put("MaxGreen", 102);
		formulaInput.put("FunMeter", 9.56);
		formulaInput.put("MaxFun", 12.35);
		formulaInput.put("ZMultiplier", 14);
		
		MvcResult mvcResult = restMockMvc.perform(post("/formula/exec/FormulaCompoundBool")
	            .contentType(APPLICATION_JSON_UTF8)
	            .content(mapper.writeValueAsBytes(formulaInput)))
	            .andExpect(status().isOk())
	            .andReturn();
		
		Map<String, Object> result = parseResult(mvcResult);
		assert(result.get("TheResult").toString().equals("12.35"));
		assert(result.get("Condition2") != null);
	}
	
	@Test
	public void execFormulaCompoundBoolDefault() throws Exception {
		formulaInput.put("Red", "NO");
		formulaInput.put("Secret", "MUBB");
		formulaInput.put("Green", 100);
		formulaInput.put("MaxGreen", 102);
		formulaInput.put("FunMeter", 9.56);
		formulaInput.put("MaxFun", 12.35);
		formulaInput.put("ZMultiplier", 14);
		
		MvcResult mvcResult = restMockMvc.perform(post("/formula/exec/FormulaCompoundBool")
	            .contentType(APPLICATION_JSON_UTF8)
	            .content(mapper.writeValueAsBytes(formulaInput)))
	            .andExpect(status().isOk())
	            .andReturn();
		
		Map<String, Object> result = parseResult(mvcResult);
		assert(result.get("TheResult").toString().equals("13.1"));
		assert(result.get("Default") != null);
	}
	
	@Test
	public void execFormulaRootSwitch() throws Exception {
		formulaInput.put("Red", "NO");
		formulaInput.put("Secret", "ZERO");
		
		MvcResult mvcResult = restMockMvc.perform(post("/formula/exec/FormulaRootSwitch")
	            .contentType(APPLICATION_JSON_UTF8)
	            .content(mapper.writeValueAsBytes(formulaInput)))
	            .andExpect(status().isOk())
	            .andReturn();
		
		Map<String, Object> result = parseResult(mvcResult);
		assert(result.get("TheResult").toString().equals("2.0"));
	}
}
