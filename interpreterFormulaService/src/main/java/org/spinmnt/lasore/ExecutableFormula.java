package org.spinmnt.lasore;

import org.spinmnt.lasore.expression.AbstractExpression;
import org.spinmnt.lasore.expression.Expression;
import org.spinmnt.lasore.expression.FormulaContext;
import org.spinmnt.lasore.expression.util.ContextParameterImpl;

import org.spinmnt.lasore.util.FormulaException;

import java.math.BigDecimal;

import java.util.HashMap;

public class ExecutableFormula {
  private org.spinmnt.lasore.expression.Expression rootExpression;
  private FormulaContext formulaContext;
  
  public ExecutableFormula() {
    super();
    this.formulaContext = new FormulaContext();
  }

  public ExecutableFormula(Expression rootExpression,
                           FormulaContext formulaContext) {
    super();
    this.rootExpression = rootExpression;
    this.formulaContext = formulaContext;
  }

  public void setRootExpression(Expression expression) {
    this.rootExpression = expression;
  }

  public Expression getRootExpression() {
    return rootExpression;
  }
  
  public void setFormulaContext(FormulaContext formulaContext) {
    this.formulaContext = formulaContext;
  }

  public FormulaContext getFormulaContext() {
    return formulaContext;
  }
  
  public HashMap<String, BigDecimal> execute(HashMap<String, Object> formulaInput, FormulaContext parentContext) throws FormulaException{
    formulaContext.reset(formulaInput, parentContext);
    rootExpression.interpret(formulaContext);
    return formulaContext.getFormulaResultMap();
  }

}
