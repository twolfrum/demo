package org.spinmnt.lasore.util;

import java.math.BigDecimal;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DataTypeUtil {
  private static DataTypeUtil myInstance;

  public enum EnumType {
    STRING,
    NUMBER,
    DATE;
  }

  public static Date parseDate(Object possibleDate) throws Exception {
    Date theDate = null;
    
    //parse input to a Date object
    DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    try {
      theDate = sdf.parse(possibleDate.toString());
    } catch (ParseException pe) {
      throw new FormulaException("Date value '" + possibleDate.toString() + "' must be in yyyy-MM-dd format"); 
    } catch (NullPointerException npe) {
      throw new FormulaException("Date value is null");         
    }
    return theDate;
  }
  
  public static Object parseNumber(Object possibleNumber, int precision, boolean roundEven, boolean roundDown, Class parsedClass) throws Exception {
    BigDecimal bd = new BigDecimal(possibleNumber.toString());
    BigDecimal rounded;
    if (roundDown) {
      rounded = bd.setScale(precision, BigDecimal.ROUND_DOWN);
    } else {
      rounded = bd.setScale(precision, BigDecimal.ROUND_HALF_UP);
    }
    if (roundEven) {
      BigDecimal[] div2 = rounded.divideAndRemainder(new BigDecimal(2));
      rounded = rounded.subtract(div2[1]);
    }
    
    if (parsedClass.getName().contains("Double")) {
      return rounded.doubleValue();  
    } else { //default is Big Decimal
      return rounded;
    }
    
    //    DecimalFormat precision2 = new DecimalFormat("#.##");
    //    Double dblVal = Double.valueOf(precision2.format(578.30116 * 0.0274));
    //    double value = ?;
    //    double result = value * 100;
    //    result = Math.round(result);
    //    result = result / 100;
  }
  
  public static Object parseValue (Object value, String dataType) throws Exception {
    Object parsedValue;
    int precision;

    if (dataType.toUpperCase().contains("NUM")) {
      // assume number dataType is in 'NUMBER(9)' format
      try {
        int p = dataType.indexOf("(");
        int pp = dataType.indexOf(")");
        precision = Integer.parseInt(dataType.substring(p+1, pp));
      } catch (Exception e) {precision = 0;}
      parsedValue = parseNumber(value, precision, false, false, BigDecimal.class);
    } else if (dataType.toUpperCase().contains("DATE")) {
      parsedValue = parseDate(value);  
    } else { // assume String/Varchar 
      parsedValue = value;
    }
    
    return parsedValue;
  }
  
  public static TypeDef parseType (String dataType) {
    TypeDef parsedType = null;
    
    if (dataType.toUpperCase().contains("CHAR") || dataType.toUpperCase().contains("STRING")) {
      parsedType = getInstance().createTypeDef(EnumType.STRING, 0);
    } else if (dataType.toUpperCase().contains("NUM")) {
      int precision = 0;
      // assume number dataType is in 'NUMBER(9)' format
      try {
        int p = dataType.indexOf("(");
        int pp = dataType.indexOf(")");
        precision = Integer.parseInt(dataType.substring(p+1, pp));
      } catch (Exception e) {} //precision defaults to 0;
      parsedType = getInstance().createTypeDef(EnumType.NUMBER, precision);
    } else if (dataType.toUpperCase().contains("DATE")) {
      parsedType = getInstance().createTypeDef(EnumType.DATE, 0);
    }
    return parsedType;
  }
  
  private static DataTypeUtil getInstance() {
    if (myInstance == null) {
      myInstance = new DataTypeUtil();  
    }
    return myInstance;
  }
  
  private TypeDef createTypeDef (DataTypeUtil.EnumType dataType, int precision) {
    return new TypeDef(dataType, precision);  
  }
  
  public class TypeDef {
    private EnumType dataType;
    private int precision;

    private TypeDef(DataTypeUtil.EnumType dataType, int precision) {
      super();
      this.dataType = dataType;
      this.precision = precision;
    }

    public DataTypeUtil.EnumType getDataType() {
      return dataType;
    }

    public int getPrecision() {
      return precision;
    }
  }
}
