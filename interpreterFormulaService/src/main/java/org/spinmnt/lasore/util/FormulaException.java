package org.spinmnt.lasore.util;

public class FormulaException extends Exception {
  public FormulaException(Throwable throwable) {
    super(throwable);
  }

  public FormulaException(String string, Throwable throwable) {
    super(string, throwable);
  }

  public FormulaException(String string) {
    super(string);
  }

  public FormulaException() {
    super();
  }
}
