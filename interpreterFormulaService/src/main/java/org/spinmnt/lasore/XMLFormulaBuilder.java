//ok
package org.spinmnt.lasore;

import org.spinmnt.lasore.definition.AbstractCondition;
import org.spinmnt.lasore.definition.AbstractExpression;
import org.spinmnt.lasore.definition.BooleanExpression;
import org.spinmnt.lasore.definition.ConditionalExpression;
import org.spinmnt.lasore.definition.Entry;
import org.spinmnt.lasore.definition.NonTerminalExpression;
import org.spinmnt.lasore.definition.TerminalExpression;
import org.spinmnt.lasore.definition.ExpressionPropertyMapping;
import org.spinmnt.lasore.definition.FormulaContextParameterMap;
import org.spinmnt.lasore.definition.Parentheses;

import org.spinmnt.lasore.expression.FormulaContext;
import org.spinmnt.lasore.expression.Expression;
import org.spinmnt.lasore.expression.Switch;
import org.spinmnt.lasore.expression.SwitchCondition;
import org.spinmnt.lasore.expression.Condition;
import org.spinmnt.lasore.expression.ConditionTest;

import org.spinmnt.lasore.expression.util.ConstantValueParameter;
import org.spinmnt.lasore.expression.util.ContextParameter;
import org.spinmnt.lasore.expression.util.FormulaResultParameter;
import org.spinmnt.lasore.expression.util.ProvidedValueParameter;

import org.spinmnt.lasore.expression.util.ReferenceParameter;
import org.spinmnt.lasore.expression.util.ExpressionPropertyUtil;
import org.spinmnt.lasore.util.FormulaException;

import java.io.File;

import java.math.BigDecimal;
import java.net.URL;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.*;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class XMLFormulaBuilder {
  private String formulaDefinitionPath;
  private Set parmNames;
    

  private static class MappingTypUtil {
    public static enum MappingTyp {
      Undefined("Undefined"),
      Value("Value"),
      Map("Map"),
      List ("List"),
      UserInput("UserInput"),
      Constant("Constant"),
      Formula("Formula");
      
      private final String strValue;
      
      MappingTyp(String strValue) {
        this.strValue = strValue;  
      }
      
      public String strValue () {
        return this.strValue;  
      }
    }
    
    public static MappingTyp getMappingTyp (String strValue) {
      MappingTyp theTyp = MappingTyp.Undefined;
      
      for (MappingTyp mt : MappingTyp.values()) {
        if (mt.strValue.equalsIgnoreCase(strValue)) {
          theTyp = mt;
          break;
        }
      }
      return theTyp;
    }
    
    public static MappingTyp getMappingTyp (ExpressionPropertyMapping mapping) {
      MappingTyp theTyp = MappingTyp.Undefined;
      if (mapping.getMap() != null) {
        theTyp = MappingTyp.Map;     
      } else if (mapping.getList() != null) {
        theTyp = MappingTyp.List;     
      } else {
        theTyp = MappingTyp.Value;
      }
      return theTyp;
    }
  }
  
  public static enum enumExpType {TERM, NONTERM, SWITCH}
  
  public XMLFormulaBuilder() {
      super();
      parmNames = new HashSet();
      // TODO: implement properties resource
      //get local path to formula definitions directory
      //formulaDefinitionPath = "?";
  }

  public ExecutableFormula buildFormulaFromDefinition
                      (String formulaDefName, String formulaVers) throws JAXBException, SAXException, FormulaException {

    //set up the environment for converting an XML formula definition into executable objects
    JAXBContext jaxbContext = JAXBContext.newInstance("org.spinmnt.lasore.definition"); 
    Unmarshaller unMarshaller = jaxbContext.createUnmarshaller();
    SchemaFactory schemaFactory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
    //Schema schema = schemaFactory.newSchema(new File(formulaDefinitionPath + "FormulaDefinition.xsd"));
    Schema schema = schemaFactory.newSchema(this.getClass().getResource("/formula_defs/FormulaDefinition.xsd"));
    unMarshaller.setSchema(schema);
    ApplicationContext springCtx = new ClassPathXmlApplicationContext("FormulaExpressionBeans.xml");

    //Instantiate the formula definition object
    //resolve version
    String resolvedVersion;
    if (formulaVers.equalsIgnoreCase("NA") ||
        formulaVers.equalsIgnoreCase("DEFAULT")) {
        resolvedVersion = "";       
    } else {
        resolvedVersion = "_" + formulaVers;    
    }
    URL xmlDocument = 
      this.getClass().getResource("/formula_defs/" + formulaDefName + resolvedVersion + ".xml");
    org.spinmnt.lasore.definition.Formula formulaModel =
      (org.spinmnt.lasore.definition.Formula)unMarshaller.unmarshal(xmlDocument);

    //build the formula tree; the returned expression is the root expression of the formula
    parmNames.clear();
    return buildFormula(formulaModel, springCtx);
  }

  private ExecutableFormula buildFormula(org.spinmnt.lasore.definition.Formula formulaModel, 
                                        ApplicationContext springCtx) throws FormulaException {
    
      //create the formula context
      FormulaContext formulaContext = buildFormulaContext(formulaModel, springCtx);

      //get the root expression
      JAXBElement<AbstractExpression> expressionElement = (JAXBElement<AbstractExpression>)formulaModel.getRootExpression().getExpression();
      AbstractExpression rootExpressionModel = (AbstractExpression)expressionElement.getValue();
      
      //recursively build the formula tree; the returned expression is the root expression of the formula
      org.spinmnt.lasore.expression.Expression rootExpBean = 
        buildFormulaExpression(rootExpressionModel, springCtx);
      
      return new ExecutableFormula(rootExpBean, formulaContext);
  }
  
  private FormulaContext buildFormulaContext (org.spinmnt.lasore.definition.Formula formulaModel, ApplicationContext springCtx) throws FormulaException {
    HashMap<String, ContextParameter> contextParameterMap = new HashMap<String, ContextParameter>();
    List<FormulaContextParameterMap.ParameterMapping> parmMapList;
    FormulaContext fc = new FormulaContext (contextParameterMap);

    try {
      //get the formula definition context parameter map
      FormulaContextParameterMap formulaDefContextParmMap =
        (FormulaContextParameterMap)formulaModel.getContextParameterMap();
      parmMapList = formulaDefContextParmMap.getParameterMapping();
    } catch (NullPointerException npe) {
      //although unlikely, it is possible for a formula to not have any context parameters 
      return fc;
    }
    
    //Create set of formula ContextParameter objects to manage execution time inputs to the formula;
    //add any input formulas to the formula context as necessary
    for (FormulaContextParameterMap.ParameterMapping parmMap : parmMapList) {
      if (parmMap.getRef() == null) {
        //new context parameter definition
        String contextParmName;
        String mappingType;
        try {
          //parm name, mapping type are required attributes for a non-ref parameter mapping
          contextParmName = parmMap.getContextParmName().toString();
          mappingType = parmMap.getMappingType().toString();
        } catch (NullPointerException npe) {
          throw new FormulaException("contextParmName and mappingType are required attributes when defining a new context parameter");
        }
        
        //check for duplicate parm definition
        if (parmNames.contains(contextParmName)) {
          throw new FormulaException("Duplicate formula definitions for context parameter '" + contextParmName + "'");    
        } else {
          parmNames.add(contextParmName);
        }
        
        switch (MappingTypUtil.getMappingTyp(mappingType)) {
        case Constant:
          contextParameterMap.put(contextParmName,
                                  new ConstantValueParameter(contextParmName,
                                                             parmMap.getDataType(),
                                                             parmMap.getSource()));
          break;
        case UserInput:
          contextParameterMap.put(contextParmName,
                                  new ProvidedValueParameter(contextParmName,
                                                             parmMap.getDataType(),
                                                             parmMap.getSource(),
                                                             parmMap.getDefaultValue()));
          break;
        case Formula:
          org.spinmnt.lasore.definition.Formula inputFormulaModel =
            parmMap.getFormula();
          ExecutableFormula inputFormula =
            buildFormula(inputFormulaModel, springCtx);
          contextParameterMap.put(contextParmName,
                                  new FormulaResultParameter(contextParmName,
                                                             inputFormula));
          break;
        default:
          throw new FormulaException("Unknown mapping type specified for context parameter '" + contextParmName + "'");
        }
      } else { // context parameter reference
        contextParameterMap.put(parmMap.getRef(), new ReferenceParameter(parmMap.getRef()));
      }
    }
    return fc;
  }
  
  private Expression buildFormulaExpression(AbstractExpression expressionModel, ApplicationContext springCtx) throws FormulaException {
    Expression fe = null;
    if (expressionModel.getClass().getName().contains("Expression")) { 
       fe = buildCalculationExpression(expressionModel, springCtx);   
    } else if (expressionModel.getClass().getName().contains("Switch")) {
       fe = buildSwitchExpression((org.spinmnt.lasore.definition.Switch)expressionModel, springCtx);   
    }
    return fe;
  }
  
  private Expression buildCalculationExpression(AbstractExpression expressionModel, ApplicationContext springCtx) throws FormulaException {
    AbstractExpressionWrapper _expressionModel = new AbstractExpressionWrapper(expressionModel);
    Expression expBean;
    
    try {
      String beanId = _expressionModel.getBeanId();
      expBean = (Expression)springCtx.getBean(beanId);
    } catch (Exception e) {
      throw new FormulaException (e); 
    }

    //formulaName, resultName, resultPrecision and roundEven are all optional attributes
    //only execute the setters if a non-null/non-default value has been specified in the 
    //formula definition to prevent over-writing what may have already been set from the 
    //spring bean config
    String optStrProp = expressionModel.getFormulaName();
    if (optStrProp != null) { 
      expBean.setFormulaName(optStrProp);
    }
    optStrProp = _expressionModel.getResultName();
    if (optStrProp != null) { 
      expBean.setResultName(optStrProp);
    }
    int optIntProp = _expressionModel.getResultPrecision();
    if (optIntProp > 0) {
      expBean.setResultPrecision(optIntProp);
    }
    boolean optBoolProp = _expressionModel.getRoundEven();
    if (optBoolProp) {
      expBean.setRoundEven(optBoolProp);
    }
    optBoolProp = _expressionModel.getRoundDown();
    if (optBoolProp) {
      expBean.setRoundDown(optBoolProp);
    }
    
    //set context dependant expression class properties
    List<ExpressionPropertyMapping> xmlProps = null;
    try {
      //the formula expression may or may not have a property map
      xmlProps = expressionModel.getProperty();
    } catch (Exception e) {}//no big deal }

    if (xmlProps != null) {
      for (ExpressionPropertyMapping xmlProp : xmlProps) {
        //the expression class property currently being processed can reference a single item value (Object)
        //or a list of values (HashMap of Name-Object pairs)
        switch (MappingTypUtil.getMappingTyp(xmlProp)) {
        case Value:
          ExpressionPropertyUtil.set(expBean, xmlProp.getName(), xmlProp.getValue().getClass(), xmlProp.getValue());
          break;
        case List:
          ExpressionPropertyUtil.set(expBean, xmlProp.getName(), List.class, (List)xmlProp.getList().getValue());
          break;
        case Map:
          //note following Map is not java.util.Map; it is an XML-based array with the element name "Map" used 
          //to make the formula definition syntax look like the Spring Bean configuration syntax
          List<Entry> pseudoMap = xmlProp.getMap().getEntry();
          Map<String, String> realMap = new HashMap<String, String>();
          for (Entry entry : pseudoMap ) {
            realMap.put(entry.getKey(), entry.getValue());  
          }
          ExpressionPropertyUtil.set(expBean, xmlProp.getName(), Map.class, realMap);
          break;
        }
      }
    }

    if (_expressionModel.getType() == XMLFormulaBuilder.enumExpType.NONTERM) {
      //recast expressionModel to access left and right operands
      NonTerminalExpression nonTermExpressionModel = (NonTerminalExpression)expressionModel;
      
      //create left operand expression 
      JAXBElement<AbstractExpression> leftElement = (JAXBElement<AbstractExpression>)nonTermExpressionModel.getLeftOperand().getExpression();
      AbstractExpression leftExpressionModel = (AbstractExpression)leftElement.getValue();
      ((org.spinmnt.lasore.expression.NonTerminalExpression)expBean).setLeftOperand(buildFormulaExpression(leftExpressionModel, springCtx));
      
      //create right operand expression 
      JAXBElement<AbstractExpression> rightElement = (JAXBElement<AbstractExpression>)nonTermExpressionModel.getRightOperand().getExpression();
      AbstractExpression rightExpressionModel = (AbstractExpression)rightElement.getValue();
      ((org.spinmnt.lasore.expression.NonTerminalExpression)expBean).setRightOperand(buildFormulaExpression(rightExpressionModel, springCtx));
    }
    return expBean;
  }
  
  private Expression buildSwitchExpression(org.spinmnt.lasore.definition.Switch switchModel, ApplicationContext springCtx) throws FormulaException {
    Switch switchExp = new Switch();
    switchExp.setFormulaName(switchModel.getFormulaName());
    
    //create each ConditionalExpression object for the switch
    List<ConditionalExpression> conditionalExps = switchModel.getConditionalExpression();
    for (ConditionalExpression ceModel : conditionalExps) {
      org.spinmnt.lasore.expression.ConditionalExpression ceExp = new org.spinmnt.lasore.expression.ConditionalExpression(); 
      
      //create the SwitchCondition object for the ConditionalExpression which may include multiple nested conditions
      ConditionalExpression.Condition ceConditionModel = ceModel.getCondition();
      List<JAXBElement<? extends AbstractCondition>> ceConditionElements = (List<JAXBElement<? extends AbstractCondition>>)ceConditionModel.getConditionElement();
      ceExp.setCondition(buildSwitchCondition(ceConditionElements, null));
      
      //create the Expression object for the ConditionalExpression
      JAXBElement<AbstractExpression> expressionElement = (JAXBElement<AbstractExpression>)ceModel.getExpression();
      AbstractExpression expressionModel = (AbstractExpression)expressionElement.getValue();
      ceExp.setExpression(buildFormulaExpression(expressionModel, springCtx));
      
      //add the ConditionalExpression object to the switch; conditional expressions are evaluated by the switch in the
      //order they are added here
      switchExp.addExpression(ceExp);
    }
    
    AbstractExpression dfltExpressionModel = null;
    try {
      //a default expression may or may not have been defined
      JAXBElement<AbstractExpression> dfltExpressionElement = (JAXBElement<AbstractExpression>)switchModel.getDefault().getExpression();
      dfltExpressionModel = (AbstractExpression)dfltExpressionElement.getValue();
    } catch (Exception e) { }

    if (dfltExpressionModel != null) {
      switchExp.setDefault(buildFormulaExpression(dfltExpressionModel, springCtx));
    }
    
    return switchExp;  
  }
  
  private SwitchCondition buildSwitchCondition (List<JAXBElement<? extends AbstractCondition>> conditionElements, String conjunctionName) throws FormulaException {
    Condition condition = new Condition(conjunctionName);
    
    for (JAXBElement<? extends AbstractCondition> abstractCondition : conditionElements) {
      //conditionElement can be either BooleanExpression or Parentheses (group of boolean expressions)
      AbstractCondition abstractModel = abstractCondition.getValue();
      if (abstractModel.getClass().getName().contains("BooleanExpression")) {
        //instantiate a ConditionTest object and configure from XML model object
        BooleanExpression boolExpModel = (BooleanExpression)abstractModel;
        
        //the boolean expression may or may not have a conjunction
        String boolConjunctName = null;
        try {
          boolConjunctName = boolExpModel.getConjunction();
        } catch (Exception e) { }
        
        ConditionTest ct = new ConditionTest(boolConjunctName);
        ct.setDataType(boolExpModel.getDataType());
        ct.setContextLeftOperand(boolExpModel.getLeftOperand());
        ct.setOperator(boolExpModel.getOperator());
        ct.setContextRightOperand(boolExpModel.getRightOperand());
        condition.addCondition(ct);
      } else if (abstractModel.getClass().getName().contains("Parentheses")) {
        Parentheses parenExpModel = (Parentheses)abstractModel;
        List<JAXBElement<? extends AbstractCondition>> parenElements = (List<JAXBElement<? extends AbstractCondition>>)parenExpModel.getConditionElement();
        String parenConjunctName = parenExpModel.getConjunction();
        condition.addCondition(buildSwitchCondition(parenElements, parenConjunctName));
      }
    }
    
    return condition;
  }
  
  private class AbstractExpressionWrapper {
    //This class is used to overcome the limitations of the inheritance/substitution group mechanisim of XML schemas.
    //It essentially functions as a "pseudo interface" for calculation type (Terminal, NonTerminal) AbstractExpressions.
    
    private AbstractExpression expressionModel;
    private enumExpType myType; 
    
    protected AbstractExpressionWrapper (AbstractExpression expressionModel) {
      this.expressionModel = expressionModel;
      if (expressionModel.getClass().getName().contains("NonTerminalExpression")) {
        myType = XMLFormulaBuilder.enumExpType.NONTERM;
      } else if (expressionModel.getClass().getName().contains("TerminalExpression")) {
        myType = XMLFormulaBuilder.enumExpType.TERM;
      }
    }
    
    protected enumExpType getType() {
      return myType;
    }
    
    protected String getBeanId() {
      String beanId = "";
      switch (myType) {
        case TERM:
          beanId = ((TerminalExpression)expressionModel).getBeanId();
          break;
        case NONTERM:
          beanId = ((NonTerminalExpression)expressionModel).getBeanId();
      }
      return beanId;
    }
    
    protected String getResultName() {
      String resultName = "";
      switch (myType) {
        case TERM:
          resultName = ((TerminalExpression)expressionModel).getResultName();
          break;
        case NONTERM:
          resultName = ((NonTerminalExpression)expressionModel).getResultName();
      }
      return resultName;
    }
    
    protected int getResultPrecision() {
      Integer resultPrecision = new Integer(0);
      switch (myType) {
        case TERM:
          resultPrecision = ((TerminalExpression)expressionModel).getResultPrecision();
          break;
        case NONTERM:
          resultPrecision = ((NonTerminalExpression)expressionModel).getResultPrecision();
      }
      try {
        return resultPrecision.intValue();
      } catch (NullPointerException npe) {
        return 0; 
      }
    }
    
    protected boolean getRoundEven() {
      String roundEven = "false";
      switch (myType) {
        case TERM:
          roundEven = ((TerminalExpression)expressionModel).getRoundEven();
          break;
        case NONTERM:
          roundEven = ((NonTerminalExpression)expressionModel).getRoundEven();
      }
      return new Boolean(roundEven).booleanValue(); 
    }

    protected boolean getRoundDown() {
        String roundDown = "false";
        switch (myType) {
          case TERM:
            roundDown = ((TerminalExpression)expressionModel).getRoundDown();
            break;
          case NONTERM:
            roundDown = ((NonTerminalExpression)expressionModel).getRoundDown();
  }
        return new Boolean(roundDown).booleanValue(); 
    }
  }
  
}
