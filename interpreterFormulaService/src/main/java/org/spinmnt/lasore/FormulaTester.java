package org.spinmnt.lasore;

import org.spinmnt.lasore.util.FormulaException;

import java.math.BigDecimal;

import java.util.HashMap;

import javax.xml.bind.JAXBException;

import org.xml.sax.SAXException;

//quick and dirty utility for executing formulas
public class FormulaTester {
    public FormulaTester() {
        super();
    }
    
    public static void main (String args[]) {
      XMLFormulaBuilder builder = new XMLFormulaBuilder();
      ExecutableFormula formula = null;

      String formulaName;
      String version;

      HashMap<String, Object> formulaInput = new HashMap<String, Object>();
      
      //formulaInput.put("StartDate", "2015-01-02");
      //formulaInput.put("EndDate", "2015-12-31");
      
      formulaName = "FormulaOne";
      formulaInput.put("Left Number", "52");
      formulaInput.put("Right Number", "17");

      try {
          formula = builder.buildFormulaFromDefinition(formulaName, "NA");
      } catch (JAXBException e) {
          e.printStackTrace();
      } catch (SAXException e) {
          e.printStackTrace();
      } catch (FormulaException e) {
          e.printStackTrace();
      }

      try {
        HashMap<String, BigDecimal> result = formula.execute(formulaInput, null);
        for (String resultName : result.keySet()) {
          System.out.println(resultName + "=" + result.get(resultName).toString());
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
}
