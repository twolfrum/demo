
package org.spinmnt.lasore.definition;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for switch complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="switch">
 *   &lt;complexContent>
 *     &lt;extension base="{org/spinmnt/lasore/formula}abstractExpression">
 *       &lt;sequence>
 *         &lt;element name="ConditionalExpression" type="{org/spinmnt/lasore/formula}conditionalExpression" maxOccurs="unbounded"/>
 *         &lt;element name="Default" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{org/spinmnt/lasore/formula}Expression"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "switch", propOrder = { "conditionalExpression", "_default" })
public class Switch extends AbstractExpression {

    @XmlElement(name = "ConditionalExpression", required = true)
    protected List<ConditionalExpression> conditionalExpression;
    @XmlElement(name = "Default")
    protected Switch.Default _default;

    /**
     * Gets the value of the conditionalExpression property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the conditionalExpression property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConditionalExpression().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConditionalExpression }
     *
     *
     */
    public List<ConditionalExpression> getConditionalExpression() {
        if (conditionalExpression == null) {
            conditionalExpression = new ArrayList<ConditionalExpression>();
        }
        return this.conditionalExpression;
    }

    /**
     * Gets the value of the default property.
     *
     * @return
     *     possible object is
     *     {@link Switch.Default }
     *
     */
    public Switch.Default getDefault() {
        return _default;
    }

    /**
     * Sets the value of the default property.
     *
     * @param value
     *     allowed object is
     *     {@link Switch.Default }
     *
     */
    public void setDefault(Switch.Default value) {
        this._default = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{org/spinmnt/lasore/formula}Expression"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "expression" })
    public static class Default {

        @XmlElementRef(name = "Expression",
                       namespace = "org/spinmnt/lasore/formula",
                       type = JAXBElement.class)
        protected JAXBElement<? extends AbstractExpression> expression;

        /**
         * Gets the value of the expression property.
         *
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link NonTerminalExpression }{@code >}
         *     {@link JAXBElement }{@code <}{@link TerminalExpression }{@code >}
         *     {@link JAXBElement }{@code <}{@link AbstractExpression }{@code >}
         *     {@link JAXBElement }{@code <}{@link Switch }{@code >}
         *
         */
        public JAXBElement<? extends AbstractExpression> getExpression() {
            return expression;
        }

        /**
         * Sets the value of the expression property.
         *
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link NonTerminalExpression }{@code >}
         *     {@link JAXBElement }{@code <}{@link TerminalExpression }{@code >}
         *     {@link JAXBElement }{@code <}{@link AbstractExpression }{@code >}
         *     {@link JAXBElement }{@code <}{@link Switch }{@code >}
         *
         */
        public void setExpression(JAXBElement<? extends AbstractExpression> value) {
            this.expression =
                    ((JAXBElement<? extends AbstractExpression>)value);
        }

    }

}
