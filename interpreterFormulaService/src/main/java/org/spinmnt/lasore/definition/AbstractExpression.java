
package org.spinmnt.lasore.definition;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for abstractExpression complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="abstractExpression">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="property" type="{org/spinmnt/lasore/formula}expressionPropertyMapping" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attGroup ref="{org/spinmnt/lasore/formula}expressionAttrbs"/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "abstractExpression", propOrder = { "property" })
@XmlSeeAlso( { Switch.class, NonTerminalExpression.class,
               TerminalExpression.class })
public class AbstractExpression {

    protected List<ExpressionPropertyMapping> property;
    @XmlAttribute
    protected String formulaName;

    /**
     * Gets the value of the property property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the property property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProperty().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ExpressionPropertyMapping }
     *
     *
     */
    public List<ExpressionPropertyMapping> getProperty() {
        if (property == null) {
            property = new ArrayList<ExpressionPropertyMapping>();
        }
        return this.property;
    }

    /**
     * Gets the value of the formulaName property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getFormulaName() {
        return formulaName;
    }

    /**
     * Sets the value of the formulaName property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setFormulaName(String value) {
        this.formulaName = value;
    }

}
