
package org.spinmnt.lasore.definition;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for abstractCondition complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="abstractCondition">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="conjunction">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *             &lt;enumeration value="And"/>
 *             &lt;enumeration value="Or"/>
 *             &lt;enumeration value="AndNot"/>
 *             &lt;enumeration value="OrNot"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "abstractCondition")
@XmlSeeAlso( { Parentheses.class, BooleanExpression.class })
public class AbstractCondition {

    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String conjunction;

    /**
     * Gets the value of the conjunction property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getConjunction() {
        return conjunction;
    }

    /**
     * Sets the value of the conjunction property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setConjunction(String value) {
        this.conjunction = value;
    }

}
