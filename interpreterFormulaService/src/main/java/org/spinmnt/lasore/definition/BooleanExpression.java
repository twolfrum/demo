
package org.spinmnt.lasore.definition;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for booleanExpression complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="booleanExpression">
 *   &lt;complexContent>
 *     &lt;extension base="{org/spinmnt/lasore/formula}abstractCondition">
 *       &lt;sequence>
 *         &lt;element name="LeftOperand" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Operator">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="EQ"/>
 *               &lt;enumeration value="NE"/>
 *               &lt;enumeration value="GT"/>
 *               &lt;enumeration value="LT"/>
 *               &lt;enumeration value="GTE"/>
 *               &lt;enumeration value="LTE"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="RightOperand" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *       &lt;attribute name="dataType" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *             &lt;pattern value="String|Date|Number\([0-9]\)"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "booleanExpression",
         propOrder = { "leftOperand", "operator", "rightOperand" })
public class BooleanExpression extends AbstractCondition {

    @XmlElement(name = "LeftOperand", required = true)
    protected String leftOperand;
    @XmlElement(name = "Operator", required = true)
    protected String operator;
    @XmlElement(name = "RightOperand", required = true)
    protected String rightOperand;
    @XmlAttribute(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String dataType;

    /**
     * Gets the value of the leftOperand property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getLeftOperand() {
        return leftOperand;
    }

    /**
     * Sets the value of the leftOperand property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setLeftOperand(String value) {
        this.leftOperand = value;
    }

    /**
     * Gets the value of the operator property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOperator() {
        return operator;
    }

    /**
     * Sets the value of the operator property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOperator(String value) {
        this.operator = value;
    }

    /**
     * Gets the value of the rightOperand property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getRightOperand() {
        return rightOperand;
    }

    /**
     * Sets the value of the rightOperand property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setRightOperand(String value) {
        this.rightOperand = value;
    }

    /**
     * Gets the value of the dataType property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDataType() {
        return dataType;
    }

    /**
     * Sets the value of the dataType property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDataType(String value) {
        this.dataType = value;
    }

}
