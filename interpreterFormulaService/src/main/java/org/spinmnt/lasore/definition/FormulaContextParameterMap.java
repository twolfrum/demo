
package org.spinmnt.lasore.definition;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 *
 *         Defines the context for executing the formula in terms of all required input parameters and the
 *         source of their respective values. Input parameters can be: supplied by the invoking process;
 *         a constant; the result of executing another (nested) formula;
 *
 *
 * <p>Java class for formulaContextParameterMap complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="formulaContextParameterMap">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ParameterMapping" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;choice minOccurs="0">
 *                     &lt;element name="Source" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element ref="{org/spinmnt/lasore/formula}Formula"/>
 *                   &lt;/choice>
 *                 &lt;/sequence>
 *                 &lt;attGroup ref="{org/spinmnt/lasore/formula}parameterMappingAttrbs"/>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "formulaContextParameterMap",
         propOrder = { "parameterMapping" })
public class FormulaContextParameterMap {

    @XmlElement(name = "ParameterMapping", required = true)
    protected List<FormulaContextParameterMap.ParameterMapping> parameterMapping;

    /**
     * Gets the value of the parameterMapping property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the parameterMapping property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParameterMapping().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FormulaContextParameterMap.ParameterMapping }
     *
     *
     */
    public List<FormulaContextParameterMap.ParameterMapping> getParameterMapping() {
        if (parameterMapping == null) {
            parameterMapping =
                    new ArrayList<FormulaContextParameterMap.ParameterMapping>();
        }
        return this.parameterMapping;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;choice minOccurs="0">
     *           &lt;element name="Source" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *           &lt;element ref="{org/spinmnt/lasore/formula}Formula"/>
     *         &lt;/choice>
     *       &lt;/sequence>
     *       &lt;attGroup ref="{org/spinmnt/lasore/formula}parameterMappingAttrbs"/>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "source", "formula" })
    public static class ParameterMapping {

        @XmlElement(name = "Source")
        protected String source;
        @XmlElement(name = "Formula")
        protected Formula formula;
        @XmlAttribute
        protected String contextParmName;
        @XmlAttribute
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        protected String mappingType;
        @XmlAttribute
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        protected String dataType;
        @XmlAttribute
        protected String defaultValue;
        @XmlAttribute
        protected String ref;

        /**
         * Gets the value of the source property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getSource() {
            return source;
        }

        /**
         * Sets the value of the source property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setSource(String value) {
            this.source = value;
        }

        /**
         * Gets the value of the formula property.
         *
         * @return
         *     possible object is
         *     {@link Formula }
         *
         */
        public Formula getFormula() {
            return formula;
        }

        /**
         * Sets the value of the formula property.
         *
         * @param value
         *     allowed object is
         *     {@link Formula }
         *
         */
        public void setFormula(Formula value) {
            this.formula = value;
        }

        /**
         * Gets the value of the contextParmName property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getContextParmName() {
            return contextParmName;
        }

        /**
         * Sets the value of the contextParmName property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setContextParmName(String value) {
            this.contextParmName = value;
        }

        /**
         * Gets the value of the mappingType property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getMappingType() {
            return mappingType;
        }

        /**
         * Sets the value of the mappingType property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setMappingType(String value) {
            this.mappingType = value;
        }

        /**
         * Gets the value of the dataType property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getDataType() {
            return dataType;
        }

        /**
         * Sets the value of the dataType property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setDataType(String value) {
            this.dataType = value;
        }

        /**
         * Gets the value of the defaultValue property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getDefaultValue() {
            return defaultValue;
        }

        /**
         * Sets the value of the defaultValue property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setDefaultValue(String value) {
            this.defaultValue = value;
        }

        /**
         * Gets the value of the ref property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getRef() {
            return ref;
        }

        /**
         * Sets the value of the ref property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setRef(String value) {
            this.ref = value;
        }

    }

}
