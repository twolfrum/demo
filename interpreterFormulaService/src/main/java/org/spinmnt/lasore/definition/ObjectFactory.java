
package org.spinmnt.lasore.definition;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the org.spinmnt.lasore.definition package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _NonTerminalExpression_QNAME =
        new QName("org/spinmnt/lasore/formula", "NonTerminalExpression");
    private final static QName _Expression_QNAME =
        new QName("org/spinmnt/lasore/formula", "Expression");
    private final static QName _ConditionElement_QNAME =
        new QName("org/spinmnt/lasore/formula", "ConditionElement");
    private final static QName _TerminalExpression_QNAME =
        new QName("org/spinmnt/lasore/formula", "TerminalExpression");
    private final static QName _Switch_QNAME =
        new QName("org/spinmnt/lasore/formula", "Switch");
    private final static QName _Parentheses_QNAME =
        new QName("org/spinmnt/lasore/formula", "Parentheses");
    private final static QName _BooleanExpression_QNAME =
        new QName("org/spinmnt/lasore/formula", "BooleanExpression");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.spinmnt.lasore.definition
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Formula }
     *
     */
    public Formula createFormula() {
        return new Formula();
    }

    /**
     * Create an instance of {@link ConditionalExpression }
     *
     */
    public ConditionalExpression createConditionalExpression() {
        return new ConditionalExpression();
    }

    /**
     * Create an instance of {@link NonTerminalExpression }
     *
     */
    public NonTerminalExpression createNonTerminalExpression() {
        return new NonTerminalExpression();
    }

    /**
     * Create an instance of {@link FormulaContextParameterMap }
     *
     */
    public FormulaContextParameterMap createFormulaContextParameterMap() {
        return new FormulaContextParameterMap();
    }

    /**
     * Create an instance of {@link Switch }
     *
     */
    public Switch createSwitch() {
        return new Switch();
    }

    /**
     * Create an instance of {@link AbstractExpression }
     *
     */
    public AbstractExpression createAbstractExpression() {
        return new AbstractExpression();
    }

    /**
     * Create an instance of {@link Formula.RootExpression }
     *
     */
    public Formula.RootExpression createFormulaRootExpression() {
        return new Formula.RootExpression();
    }

    /**
     * Create an instance of {@link Parentheses }
     *
     */
    public Parentheses createParentheses() {
        return new Parentheses();
    }

    /**
     * Create an instance of {@link AbstractCondition }
     *
     */
    public AbstractCondition createAbstractCondition() {
        return new AbstractCondition();
    }

    /**
     * Create an instance of {@link BooleanExpression }
     *
     */
    public BooleanExpression createBooleanExpression() {
        return new BooleanExpression();
    }

    /**
     * Create an instance of {@link Entry }
     *
     */
    public Entry createEntry() {
        return new Entry();
    }

    /**
     * Create an instance of {@link Map }
     *
     */
    public Map createMap() {
        return new Map();
    }

    /**
     * Create an instance of {@link List }
     *
     */
    public List createList() {
        return new List();
    }

    /**
     * Create an instance of {@link TerminalExpression }
     *
     */
    public TerminalExpression createTerminalExpression() {
        return new TerminalExpression();
    }

    /**
     * Create an instance of {@link ExpressionPropertyMapping }
     *
     */
    public ExpressionPropertyMapping createExpressionPropertyMapping() {
        return new ExpressionPropertyMapping();
    }

    /**
     * Create an instance of {@link ConditionalExpression.Condition }
     *
     */
    public ConditionalExpression.Condition createConditionalExpressionCondition() {
        return new ConditionalExpression.Condition();
    }

    /**
     * Create an instance of {@link NonTerminalExpression.LeftOperand }
     *
     */
    public NonTerminalExpression.LeftOperand createNonTerminalExpressionLeftOperand() {
        return new NonTerminalExpression.LeftOperand();
    }

    /**
     * Create an instance of {@link NonTerminalExpression.RightOperand }
     *
     */
    public NonTerminalExpression.RightOperand createNonTerminalExpressionRightOperand() {
        return new NonTerminalExpression.RightOperand();
    }

    /**
     * Create an instance of {@link FormulaContextParameterMap.ParameterMapping }
     *
     */
    public FormulaContextParameterMap.ParameterMapping createFormulaContextParameterMapParameterMapping() {
        return new FormulaContextParameterMap.ParameterMapping();
    }

    /**
     * Create an instance of {@link Switch.Default }
     *
     */
    public Switch.Default createSwitchDefault() {
        return new Switch.Default();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NonTerminalExpression }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "org/spinmnt/lasore/formula",
                    name = "NonTerminalExpression",
                    substitutionHeadNamespace = "org/spinmnt/lasore/formula",
                    substitutionHeadName = "Expression")
    public JAXBElement<NonTerminalExpression> createNonTerminalExpression(NonTerminalExpression value) {
        return new JAXBElement<NonTerminalExpression>(_NonTerminalExpression_QNAME,
                                                      NonTerminalExpression.class,
                                                      null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AbstractExpression }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "org/spinmnt/lasore/formula",
                    name = "Expression")
    public JAXBElement<AbstractExpression> createExpression(AbstractExpression value) {
        return new JAXBElement<AbstractExpression>(_Expression_QNAME,
                                                   AbstractExpression.class,
                                                   null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AbstractCondition }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "org/spinmnt/lasore/formula",
                    name = "ConditionElement")
    public JAXBElement<AbstractCondition> createConditionElement(AbstractCondition value) {
        return new JAXBElement<AbstractCondition>(_ConditionElement_QNAME,
                                                  AbstractCondition.class,
                                                  null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TerminalExpression }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "org/spinmnt/lasore/formula",
                    name = "TerminalExpression",
                    substitutionHeadNamespace = "org/spinmnt/lasore/formula",
                    substitutionHeadName = "Expression")
    public JAXBElement<TerminalExpression> createTerminalExpression(TerminalExpression value) {
        return new JAXBElement<TerminalExpression>(_TerminalExpression_QNAME,
                                                   TerminalExpression.class,
                                                   null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Switch }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "org/spinmnt/lasore/formula", name = "Switch",
                    substitutionHeadNamespace = "org/spinmnt/lasore/formula",
                    substitutionHeadName = "Expression")
    public JAXBElement<Switch> createSwitch(Switch value) {
        return new JAXBElement<Switch>(_Switch_QNAME, Switch.class, null,
                                       value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Parentheses }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "org/spinmnt/lasore/formula",
                    name = "Parentheses",
                    substitutionHeadNamespace = "org/spinmnt/lasore/formula",
                    substitutionHeadName = "ConditionElement")
    public JAXBElement<Parentheses> createParentheses(Parentheses value) {
        return new JAXBElement<Parentheses>(_Parentheses_QNAME,
                                            Parentheses.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BooleanExpression }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "org/spinmnt/lasore/formula",
                    name = "BooleanExpression",
                    substitutionHeadNamespace = "org/spinmnt/lasore/formula",
                    substitutionHeadName = "ConditionElement")
    public JAXBElement<BooleanExpression> createBooleanExpression(BooleanExpression value) {
        return new JAXBElement<BooleanExpression>(_BooleanExpression_QNAME,
                                                  BooleanExpression.class,
                                                  null, value);
    }

}
