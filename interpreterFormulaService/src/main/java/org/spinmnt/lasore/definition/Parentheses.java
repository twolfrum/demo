
package org.spinmnt.lasore.definition;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for parentheses complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="parentheses">
 *   &lt;complexContent>
 *     &lt;extension base="{org/spinmnt/lasore/formula}abstractCondition">
 *       &lt;sequence>
 *         &lt;element ref="{org/spinmnt/lasore/formula}ConditionElement" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "parentheses", propOrder = { "conditionElement" })
public class Parentheses extends AbstractCondition {

    @XmlElementRef(name = "ConditionElement",
                   namespace = "org/spinmnt/lasore/formula",
                   type = JAXBElement.class)
    protected List<JAXBElement<? extends AbstractCondition>> conditionElement;

    /**
     * Gets the value of the conditionElement property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the conditionElement property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConditionElement().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link AbstractCondition }{@code >}
     * {@link JAXBElement }{@code <}{@link Parentheses }{@code >}
     * {@link JAXBElement }{@code <}{@link BooleanExpression }{@code >}
     *
     *
     */
    public List<JAXBElement<? extends AbstractCondition>> getConditionElement() {
        if (conditionElement == null) {
            conditionElement =
                    new ArrayList<JAXBElement<? extends AbstractCondition>>();
        }
        return this.conditionElement;
    }

}
