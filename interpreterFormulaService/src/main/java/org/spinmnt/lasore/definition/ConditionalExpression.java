
package org.spinmnt.lasore.definition;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for conditionalExpression complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="conditionalExpression">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Condition">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{org/spinmnt/lasore/formula}ConditionElement" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element ref="{org/spinmnt/lasore/formula}Expression"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "conditionalExpression",
         propOrder = { "condition", "expression" })
public class ConditionalExpression {

    @XmlElement(name = "Condition", required = true)
    protected ConditionalExpression.Condition condition;
    @XmlElementRef(name = "Expression",
                   namespace = "org/spinmnt/lasore/formula",
                   type = JAXBElement.class)
    protected JAXBElement<? extends AbstractExpression> expression;

    /**
     * Gets the value of the condition property.
     *
     * @return
     *     possible object is
     *     {@link ConditionalExpression.Condition }
     *
     */
    public ConditionalExpression.Condition getCondition() {
        return condition;
    }

    /**
     * Sets the value of the condition property.
     *
     * @param value
     *     allowed object is
     *     {@link ConditionalExpression.Condition }
     *
     */
    public void setCondition(ConditionalExpression.Condition value) {
        this.condition = value;
    }

    /**
     * Gets the value of the expression property.
     *
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TerminalExpression }{@code >}
     *     {@link JAXBElement }{@code <}{@link Switch }{@code >}
     *     {@link JAXBElement }{@code <}{@link NonTerminalExpression }{@code >}
     *     {@link JAXBElement }{@code <}{@link AbstractExpression }{@code >}
     *
     */
    public JAXBElement<? extends AbstractExpression> getExpression() {
        return expression;
    }

    /**
     * Sets the value of the expression property.
     *
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TerminalExpression }{@code >}
     *     {@link JAXBElement }{@code <}{@link Switch }{@code >}
     *     {@link JAXBElement }{@code <}{@link NonTerminalExpression }{@code >}
     *     {@link JAXBElement }{@code <}{@link AbstractExpression }{@code >}
     *
     */
    public void setExpression(JAXBElement<? extends AbstractExpression> value) {
        this.expression = ((JAXBElement<? extends AbstractExpression>)value);
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{org/spinmnt/lasore/formula}ConditionElement" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "conditionElement" })
    public static class Condition {

        @XmlElementRef(name = "ConditionElement",
                       namespace = "org/spinmnt/lasore/formula",
                       type = JAXBElement.class)
        protected List<JAXBElement<? extends AbstractCondition>> conditionElement;

        /**
         * Gets the value of the conditionElement property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the conditionElement property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getConditionElement().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link JAXBElement }{@code <}{@link AbstractCondition }{@code >}
         * {@link JAXBElement }{@code <}{@link Parentheses }{@code >}
         * {@link JAXBElement }{@code <}{@link BooleanExpression }{@code >}
         *
         *
         */
        public List<JAXBElement<? extends AbstractCondition>> getConditionElement() {
            if (conditionElement == null) {
                conditionElement =
                        new ArrayList<JAXBElement<? extends AbstractCondition>>();
            }
            return this.conditionElement;
        }

    }

}
