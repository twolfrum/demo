
package org.spinmnt.lasore.definition;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for terminalExpression complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="terminalExpression">
 *   &lt;complexContent>
 *     &lt;extension base="{org/spinmnt/lasore/formula}abstractExpression">
 *       &lt;attGroup ref="{org/spinmnt/lasore/formula}calculationExpressionAttrbs"/>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "terminalExpression")
public class TerminalExpression extends AbstractExpression {

    @XmlAttribute(required = true)
    protected String beanId;
    @XmlAttribute
    protected String resultName;
    @XmlAttribute
    protected Integer resultPrecision;
    @XmlAttribute
    protected String roundEven;
    @XmlAttribute
    protected String roundDown;

    /**
     * Gets the value of the beanId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getBeanId() {
        return beanId;
    }

    /**
     * Sets the value of the beanId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setBeanId(String value) {
        this.beanId = value;
    }

    /**
     * Gets the value of the resultName property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getResultName() {
        return resultName;
    }

    /**
     * Sets the value of the resultName property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setResultName(String value) {
        this.resultName = value;
    }

    /**
     * Gets the value of the resultPrecision property.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     *
     */
    public Integer getResultPrecision() {
        return resultPrecision;
    }

    /**
     * Sets the value of the resultPrecision property.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     *
     */
    public void setResultPrecision(Integer value) {
        this.resultPrecision = value;
    }

    /**
     * Gets the value of the roundEven property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getRoundEven() {
        if (roundEven == null) {
            return "false";
        } else {
            return roundEven;
        }
    }

    /**
     * Sets the value of the roundEven property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setRoundEven(String value) {
        this.roundEven = value;
    }

    /**
     * Gets the value of the roundDown property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getRoundDown() {
        if (roundDown == null) {
            return "false";
        } else {
            return roundDown;
        }
    }

    /**
     * Sets the value of the roundDown property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setRoundDown(String value) {
        this.roundDown = value;
    }

}
