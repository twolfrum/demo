
package org.spinmnt.lasore.definition;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ContextParameterMap" type="{org/spinmnt/lasore/formula}formulaContextParameterMap" minOccurs="0"/>
 *         &lt;element name="RootExpression">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{org/spinmnt/lasore/formula}Expression"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attGroup ref="{org/spinmnt/lasore/formula}formulaAttrbs"/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "contextParameterMap", "rootExpression" })
@XmlRootElement(name = "Formula")
public class Formula {

    @XmlElement(name = "ContextParameterMap")
    protected FormulaContextParameterMap contextParameterMap;
    @XmlElement(name = "RootExpression", required = true)
    protected Formula.RootExpression rootExpression;
    @XmlAttribute
    protected String version;
    @XmlAttribute
    protected String calculationType;

    /**
     * Gets the value of the contextParameterMap property.
     *
     * @return
     *     possible object is
     *     {@link FormulaContextParameterMap }
     *
     */
    public FormulaContextParameterMap getContextParameterMap() {
        return contextParameterMap;
    }

    /**
     * Sets the value of the contextParameterMap property.
     *
     * @param value
     *     allowed object is
     *     {@link FormulaContextParameterMap }
     *
     */
    public void setContextParameterMap(FormulaContextParameterMap value) {
        this.contextParameterMap = value;
    }

    /**
     * Gets the value of the rootExpression property.
     *
     * @return
     *     possible object is
     *     {@link Formula.RootExpression }
     *
     */
    public Formula.RootExpression getRootExpression() {
        return rootExpression;
    }

    /**
     * Sets the value of the rootExpression property.
     *
     * @param value
     *     allowed object is
     *     {@link Formula.RootExpression }
     *
     */
    public void setRootExpression(Formula.RootExpression value) {
        this.rootExpression = value;
    }

    /**
     * Gets the value of the version property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Gets the value of the calculationType property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCalculationType() {
        return calculationType;
    }

    /**
     * Sets the value of the calculationType property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCalculationType(String value) {
        this.calculationType = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{org/spinmnt/lasore/formula}Expression"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "expression" })
    public static class RootExpression {

        @XmlElementRef(name = "Expression",
                       namespace = "org/spinmnt/lasore/formula",
                       type = JAXBElement.class)
        protected JAXBElement<? extends AbstractExpression> expression;

        /**
         * Gets the value of the expression property.
         *
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link NonTerminalExpression }{@code >}
         *     {@link JAXBElement }{@code <}{@link TerminalExpression }{@code >}
         *     {@link JAXBElement }{@code <}{@link AbstractExpression }{@code >}
         *     {@link JAXBElement }{@code <}{@link Switch }{@code >}
         *
         */
        public JAXBElement<? extends AbstractExpression> getExpression() {
            return expression;
        }

        /**
         * Sets the value of the expression property.
         *
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link NonTerminalExpression }{@code >}
         *     {@link JAXBElement }{@code <}{@link TerminalExpression }{@code >}
         *     {@link JAXBElement }{@code <}{@link AbstractExpression }{@code >}
         *     {@link JAXBElement }{@code <}{@link Switch }{@code >}
         *
         */
        public void setExpression(JAXBElement<? extends AbstractExpression> value) {
            this.expression =
                    ((JAXBElement<? extends AbstractExpression>)value);
        }

    }

}
