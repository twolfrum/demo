
package org.spinmnt.lasore.definition;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for nonTerminalExpression complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="nonTerminalExpression">
 *   &lt;complexContent>
 *     &lt;extension base="{org/spinmnt/lasore/formula}abstractExpression">
 *       &lt;sequence>
 *         &lt;element name="LeftOperand">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{org/spinmnt/lasore/formula}Expression"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="RightOperand">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{org/spinmnt/lasore/formula}Expression"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attGroup ref="{org/spinmnt/lasore/formula}calculationExpressionAttrbs"/>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nonTerminalExpression",
         propOrder = { "leftOperand", "rightOperand" })
public class NonTerminalExpression extends AbstractExpression {

    @XmlElement(name = "LeftOperand", required = true)
    protected NonTerminalExpression.LeftOperand leftOperand;
    @XmlElement(name = "RightOperand", required = true)
    protected NonTerminalExpression.RightOperand rightOperand;
    @XmlAttribute(required = true)
    protected String beanId;
    @XmlAttribute
    protected String resultName;
    @XmlAttribute
    protected Integer resultPrecision;
    @XmlAttribute
    protected String roundEven;
    @XmlAttribute
    protected String roundDown;

    /**
     * Gets the value of the leftOperand property.
     *
     * @return
     *     possible object is
     *     {@link NonTerminalExpression.LeftOperand }
     *
     */
    public NonTerminalExpression.LeftOperand getLeftOperand() {
        return leftOperand;
    }

    /**
     * Sets the value of the leftOperand property.
     *
     * @param value
     *     allowed object is
     *     {@link NonTerminalExpression.LeftOperand }
     *
     */
    public void setLeftOperand(NonTerminalExpression.LeftOperand value) {
        this.leftOperand = value;
    }

    /**
     * Gets the value of the rightOperand property.
     *
     * @return
     *     possible object is
     *     {@link NonTerminalExpression.RightOperand }
     *
     */
    public NonTerminalExpression.RightOperand getRightOperand() {
        return rightOperand;
    }

    /**
     * Sets the value of the rightOperand property.
     *
     * @param value
     *     allowed object is
     *     {@link NonTerminalExpression.RightOperand }
     *
     */
    public void setRightOperand(NonTerminalExpression.RightOperand value) {
        this.rightOperand = value;
    }

    /**
     * Gets the value of the beanId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getBeanId() {
        return beanId;
    }

    /**
     * Sets the value of the beanId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setBeanId(String value) {
        this.beanId = value;
    }

    /**
     * Gets the value of the resultName property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getResultName() {
        return resultName;
    }

    /**
     * Sets the value of the resultName property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setResultName(String value) {
        this.resultName = value;
    }

    /**
     * Gets the value of the resultPrecision property.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     *
     */
    public Integer getResultPrecision() {
        return resultPrecision;
    }

    /**
     * Sets the value of the resultPrecision property.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     *
     */
    public void setResultPrecision(Integer value) {
        this.resultPrecision = value;
    }

    /**
     * Gets the value of the roundEven property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getRoundEven() {
        if (roundEven == null) {
            return "false";
        } else {
            return roundEven;
        }
    }

    /**
     * Sets the value of the roundEven property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setRoundEven(String value) {
        this.roundEven = value;
    }

    /**
     * Gets the value of the roundDown property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getRoundDown() {
        if (roundDown == null) {
            return "false";
        } else {
            return roundDown;
        }
    }

    /**
     * Sets the value of the roundDown property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setRoundDown(String value) {
        this.roundDown = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{org/spinmnt/lasore/formula}Expression"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "expression" })
    public static class LeftOperand {

        @XmlElementRef(name = "Expression",
                       namespace = "org/spinmnt/lasore/formula",
                       type = JAXBElement.class)
        protected JAXBElement<? extends AbstractExpression> expression;

        /**
         * Gets the value of the expression property.
         *
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link NonTerminalExpression }{@code >}
         *     {@link JAXBElement }{@code <}{@link TerminalExpression }{@code >}
         *     {@link JAXBElement }{@code <}{@link AbstractExpression }{@code >}
         *     {@link JAXBElement }{@code <}{@link Switch }{@code >}
         *
         */
        public JAXBElement<? extends AbstractExpression> getExpression() {
            return expression;
        }

        /**
         * Sets the value of the expression property.
         *
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link NonTerminalExpression }{@code >}
         *     {@link JAXBElement }{@code <}{@link TerminalExpression }{@code >}
         *     {@link JAXBElement }{@code <}{@link AbstractExpression }{@code >}
         *     {@link JAXBElement }{@code <}{@link Switch }{@code >}
         *
         */
        public void setExpression(JAXBElement<? extends AbstractExpression> value) {
            this.expression =
                    ((JAXBElement<? extends AbstractExpression>)value);
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{org/spinmnt/lasore/formula}Expression"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "expression" })
    public static class RightOperand {

        @XmlElementRef(name = "Expression",
                       namespace = "org/spinmnt/lasore/formula",
                       type = JAXBElement.class)
        protected JAXBElement<? extends AbstractExpression> expression;

        /**
         * Gets the value of the expression property.
         *
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link NonTerminalExpression }{@code >}
         *     {@link JAXBElement }{@code <}{@link TerminalExpression }{@code >}
         *     {@link JAXBElement }{@code <}{@link AbstractExpression }{@code >}
         *     {@link JAXBElement }{@code <}{@link Switch }{@code >}
         *
         */
        public JAXBElement<? extends AbstractExpression> getExpression() {
            return expression;
        }

        /**
         * Sets the value of the expression property.
         *
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link NonTerminalExpression }{@code >}
         *     {@link JAXBElement }{@code <}{@link TerminalExpression }{@code >}
         *     {@link JAXBElement }{@code <}{@link AbstractExpression }{@code >}
         *     {@link JAXBElement }{@code <}{@link Switch }{@code >}
         *
         */
        public void setExpression(JAXBElement<? extends AbstractExpression> value) {
            this.expression =
                    ((JAXBElement<? extends AbstractExpression>)value);
        }

    }

}
