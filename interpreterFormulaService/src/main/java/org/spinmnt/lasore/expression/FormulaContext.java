//ok BD used in results only
package org.spinmnt.lasore.expression;

import org.spinmnt.lasore.expression.util.ContextParameter;
import org.spinmnt.lasore.expression.util.FormulaResultParameter;
import org.spinmnt.lasore.util.FormulaException;
import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FormulaContext {
  private HashMap<String, ContextParameter> contextParameterMap;
  //contextParameterMap key = name of ContextParameter
  //contextParameterMap value = ContextParameter object
  private HashMap<String, BigDecimal> formulaResultMap;
  private FormulaContext parentContext;
  private long execRequestId;
  private boolean isSorted;
  private List<String> parmSequence;

  public FormulaContext() {
    super();
    this.contextParameterMap = new HashMap<String, ContextParameter>();
    this.formulaResultMap = new HashMap<String, BigDecimal>();
    this.parentContext = null;
  }

  public FormulaContext(HashMap<String, ContextParameter> contextParameterMap) {
    super();
    this.contextParameterMap = contextParameterMap;
    this.formulaResultMap = new HashMap<String, BigDecimal>();
    this.parentContext = null;
  }

  public void setContextParameterMap(HashMap<String, ContextParameter> contextParameterMap) {
    this.contextParameterMap = contextParameterMap;
  }

  public HashMap<String, ContextParameter> getContextParameterMap() {
    return contextParameterMap;
  }

  public HashMap<String, BigDecimal> getFormulaResultMap() {
    return formulaResultMap;
  }

  public ContextParameter getContextParameter(String parameterName) {
    ContextParameter cp;
    try {
      cp = this.contextParameterMap.get(parameterName);
      if (cp.getClass().getName().contains("Reference")) {
      //not sure if following is necessary
      //if (cp == null || cp.getClass().getName().contains("Reference")) {
        cp = this.parentContext.getContextParameter(parameterName);  
      }
    } catch (Exception e) {
      return null;
    } 
    return cp;
  }
  
  public void reset(HashMap<String, Object> formulaInput, FormulaContext parentContext) throws FormulaException {
    formulaResultMap.clear();  
    this.parentContext = parentContext;
    
    try {
      if (execRequestId == parentContext.getExecRequestId()) {
        return;
      } else {
        execRequestId = parentContext.getExecRequestId(); 
      }
    } catch (NullPointerException npe) {
      //ParentContext will not exist if this is the 'root' context
      //of the formula which means this is the start of a new execution
       execRequestId = System.currentTimeMillis();
    }  
    
    sortParms(parentContext);
    
    for (String parmName : parmSequence) {
      contextParameterMap.get(parmName).reset(formulaInput, this);  
    }
  } 

  public BigDecimal getResult(String resultName) {
    BigDecimal result;
    try {
      result = this.formulaResultMap.get(resultName);
      //not sure if following is necessary/correct but included for consistency
      if (result == null) {
        result = this.parentContext.getResult(resultName);  
      }
    } catch (Exception e) {
      return new BigDecimal(0);
    }
    return result;
  }
  
  public void setResult (String resultName, Double value) {
    this.formulaResultMap.put(resultName, BigDecimal.valueOf(value));
    if (this.parentContext != null) {
      this.parentContext.setResult(resultName, value);  
    }
  }

  public long getExecRequestId() {
    return execRequestId;
  }
  
  private void sortParms(FormulaContext parentContext) throws FormulaException {
    parmSequence = new ArrayList<String>();
    
    //first add all non-reference, non-formula parms to the sequence
    for (String parmName : contextParameterMap.keySet()) {
      ContextParameter cp = contextParameterMap.get(parmName);
      if (!cp.getClass().getName().contains("Reference") &&
          !cp.getClass().getName().contains("Formula")) {
        parmSequence.add(parmName);
      }
    }
    
    //next add all reference parms to the sequence
    for (String parmName : contextParameterMap.keySet()) {
      ContextParameter cp = contextParameterMap.get(parmName);
      if (cp.getClass().getName().contains("Reference")) {
        parmSequence.add(parmName);
      }
    }
    
    //now check for dependencies between formula result parameters
    List<FormulaResultParameter> formulaParms = new ArrayList<FormulaResultParameter>();
    for (String parmName : contextParameterMap.keySet()) {
      ContextParameter cp = contextParameterMap.get(parmName);
      if (cp.getClass().getName().contains("Formula")) {
        formulaParms.add((FormulaResultParameter)cp);
      }
    }
    
    //Continuously loop thru formula result parms. For each iteration,
    //remove the parms from the list whose dependant parms can be satisfied
    //with either a parm from this context or from the parent context
    
    while (formulaParms.size() > 0) {
      int startSize = formulaParms.size();
      
      for (FormulaResultParameter frp : formulaParms) {
        List<String> formulaParmDepends = frp.getContextDependencies();
        int satisfiedDepends = 0;
        for (String dependantParm : formulaParmDepends) {
          if (parmSequence.contains(dependantParm)) {
            satisfiedDepends++;    
          } else {
            try {
              ContextParameter cp = parentContext.getContextParameter(dependantParm);
              if (cp != null) {
                satisfiedDepends++;   
              }
            } catch (NullPointerException npe){} //parentContext may not exist
          }
        }
        if (satisfiedDepends == formulaParmDepends.size()) {
          parmSequence.add(frp.getParameterName());
          formulaParms.remove(frp);
          break;
        }
      }
      
      if (startSize == formulaParms.size()) {
        throw new FormulaException("Invalid formula definition - Circular dependancy between nested formulas detected");       
      }
    }
  }
}
