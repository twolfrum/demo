package org.spinmnt.lasore.expression.ImplBeans;

import org.spinmnt.lasore.expression.FormulaContext;
import org.spinmnt.lasore.expression.NonTerminalExpression;
import org.spinmnt.lasore.expression.util.ContextParameterImpl;

import org.spinmnt.lasore.util.FormulaException;

import java.text.DecimalFormat;

import java.util.HashMap;

public class Discount extends NonTerminalExpression {
  public Discount() {
  }

  public Object interpret(FormulaContext formulaContext) throws FormulaException {
    super.interpret(formulaContext);
    
    double result = 0;
    double baseValue = (Double)this.getLeftOperand().interpret(formulaContext);
    double discountPercent = (Double)this.getRightOperand().interpret(formulaContext);
    
    try {
      if (discountPercent == 0) {
        result = baseValue;  
      } else {
        result = baseValue - (baseValue * discountPercent/100.0);
      }
    } catch (Exception e) {
      String msg = "Error calculating discount caused by " + e.getMessage();
      if (getResultName() != null) {
        msg = msg + " for '" + getResultName() + "'";    
      }
      throw new FormulaException(msg);  
    }

    return processResult(formulaContext, result);
  }

  @Override
  public String getFormulaName() {
    try {
      return super.getFormulaName().toString();
    } catch (NullPointerException npe) {
      return "Discounted By";
    }
  }
}
