//ok
package org.spinmnt.lasore.expression;

import org.spinmnt.lasore.expression.util.ContextClientImpl;
import org.spinmnt.lasore.expression.util.ContextParameterImpl;

import org.spinmnt.lasore.util.DataTypeUtil;

import org.spinmnt.lasore.util.FormulaException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.HashMap;

public abstract class AbstractExpression extends ContextClientImpl implements Expression {
  private String formulaName;
  private String resultName;
  private int resultPrecision;
  private boolean roundEven;
  private boolean roundDown;

  public AbstractExpression() {
    super();
    this.formulaName = null;
    this.resultName = null;
    this.roundEven = false;
    this.roundDown = false;
    this.resultPrecision = 0;
  }

  public Object interpret(FormulaContext formulaContext) throws FormulaException {
    applyContext (formulaContext);
    return null;
  }
  
  public abstract String getDescription();
  
  protected Double processResult (FormulaContext formulaContext, double result) throws FormulaException {
    String resultAsString = new Double(result).toString();
    Double processedResult;
    try {
      processedResult = (Double) DataTypeUtil.parseNumber(resultAsString, this.resultPrecision, this.roundEven, this.roundDown, Double.class);
    } catch (Exception e) {
      throw new FormulaException ("Error processing result value '" + resultAsString + "' for " + this.resultName);  
    }
    //an empty resultName attribute for the expression indicates the formula definition is not interested in
    //managing the result of this expression
    if (this.resultName != null) {
      formulaContext.setResult(this.resultName, processedResult);
    }
    return processedResult;
  }
  
  public void setFormulaName(String formulaName) {
    this.formulaName = formulaName;
  }

  public String getFormulaName() {
    return formulaName;
  }
  
  public void setResultName(String resultName) {
    this.resultName = resultName;
  }

  public String getResultName() {
    return resultName;
  }

  public void setResultPrecision(int resultPrecision) {
    this.resultPrecision = resultPrecision;
  }

  public int getResultPrecision() {
    return resultPrecision;
  }

  public void setRoundEven(boolean roundEven) {
    this.roundEven = roundEven;
  }

  public boolean getRoundEven() {
    return roundEven;
  }

  public void setRoundDown(boolean roundDown) {
    this.roundDown = roundDown;
  }
    
  public boolean getRoundDown() {
    return roundDown;
  }
}
