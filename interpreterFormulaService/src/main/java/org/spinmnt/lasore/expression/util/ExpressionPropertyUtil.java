package org.spinmnt.lasore.expression.util;

import org.spinmnt.lasore.expression.Expression;

import org.spinmnt.lasore.util.FormulaException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ExpressionPropertyUtil {
  
  public static void set(Object expBean, String propertyName, Class propertyClass, Object value) throws FormulaException {
    String exFmt = "%1$s Exception encountered attempting to call method '%2$s' on '" + expBean.getClass().getName() + "'";
    String methodName = "";
    Method propMeth = null;
    try {
      methodName = Character.toUpperCase(propertyName.charAt(0)) + propertyName.substring(1);
      methodName = "set" + methodName;
      propMeth = expBean.getClass().getDeclaredMethod(methodName, new Class[] { propertyClass });
    } catch (SecurityException se) {
      // TODO: Add catch code
      se.printStackTrace();
      throw new FormulaException(String.format(exFmt, new Object[] {"Security", methodName}));
    } catch (NoSuchMethodException nsme) {
      // TODO: Add catch code
      nsme.printStackTrace();
      throw new FormulaException(String.format(exFmt, new Object[] {"No Such Method", methodName}));
    }
    
    //set the expression bean property 
    try {
      propMeth.setAccessible(true);
      propMeth.invoke(expBean, new Object[] {value});
    } catch (IllegalAccessException e) {
      e.printStackTrace();
      throw new FormulaException(String.format(exFmt, new Object[] {"Illegal Access", propMeth.getName()}));
    } catch (InvocationTargetException e) {
      e.printStackTrace();
      throw new FormulaException(String.format(exFmt, new Object[] {"Invocation Target", propMeth.getName()}));
    }
  }
}
