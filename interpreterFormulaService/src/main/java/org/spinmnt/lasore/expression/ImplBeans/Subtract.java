package org.spinmnt.lasore.expression.ImplBeans;

import org.spinmnt.lasore.expression.FormulaContext;
import org.spinmnt.lasore.expression.NonTerminalExpression;
import org.spinmnt.lasore.util.FormulaException;

public class Subtract extends NonTerminalExpression {
  public Subtract() {
  }
  
  public Object interpret(FormulaContext formulaContext) throws FormulaException {
    super.interpret(formulaContext);

    double result = 0;
    double left = (Double)this.getLeftOperand().interpret(formulaContext);
    double right = (Double)this.getRightOperand().interpret(formulaContext);
    
    try {
      result = left - right;
    } catch (Exception e) {
      String msg = "Subtraction error caused by " + e.getMessage();
      if (getResultName() != null) {
        msg = msg + " while calculating '" + getResultName() + "'";    
      }
      throw new FormulaException(msg);  
    }
        
    return processResult(formulaContext, result);
  }
  
  @Override
  public String getFormulaName() {
    try {
      return super.getFormulaName().toString();
    } catch (NullPointerException npe) {
      return "-";
    }
  }
}
