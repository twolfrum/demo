package org.spinmnt.lasore.expression;

public abstract class TerminalExpression extends AbstractExpression {
  public final String getDescription() {
    return getFormulaName();
  }  
}
