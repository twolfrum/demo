package org.spinmnt.lasore.expression.ImplBeans;

import org.spinmnt.lasore.expression.FormulaContext;
import org.spinmnt.lasore.expression.NonTerminalExpression;
import org.spinmnt.lasore.util.FormulaException;

public class Divide extends NonTerminalExpression {
  public Divide() {
  }
  
  public Object interpret(FormulaContext formulaContext) throws FormulaException {
    super.interpret(formulaContext);

    double result = 0;
    double left = (Double)this.getLeftOperand().interpret(formulaContext);
    double right = (Double)this.getRightOperand().interpret(formulaContext);
    
    String msgExt = "";
    if (getResultName() != null) {
      msgExt = " while calculating '" + getResultName() + "'";    
    }
    
    //default java lang message for zero divide condition is not obvious - treat separately
    if (right == 0) {
      throw new FormulaException("Zero divide error encountered" + msgExt);    
    }
    
    try {
      result = left / right;
    } catch (Exception e) {
      throw new FormulaException("Division error caused by " + e.getMessage() + msgExt);  
    }
        
    return processResult(formulaContext, result);
  }
  
  @Override
  public String getFormulaName() {
    try {
      return super.getFormulaName().toString();
    } catch (NullPointerException npe) {
      return "/";
    }
  }
}
