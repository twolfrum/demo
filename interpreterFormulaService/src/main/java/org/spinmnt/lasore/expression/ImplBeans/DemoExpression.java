package org.spinmnt.lasore.expression.ImplBeans;

import java.math.BigDecimal;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

import org.spinmnt.lasore.expression.FormulaContext;
import org.spinmnt.lasore.expression.TerminalExpression;
import org.spinmnt.lasore.util.FormulaException;

/*
 * A stub class to illustrate what a typical TerminalExpression bean implementation
 * might look like. The value returned by the interpret() method could be the result of for example,
 * a database lookup, an internal/external service invocation or a complex algorithm. Regardless, the 
 * derived value of the expression is a function of the input parameters specified in the formulaContext.    
 */

public class DemoExpression extends TerminalExpression {
	
	//example: Provision a db repository to query for a value based on the input parameters
	//@Autowired
	//private SomeRepository theRepository;
	
	//example: Inject a service proxy component for invoking a REST service request 
	//where for example, the query parms of the request are based on the the input parameters
	//@Inject
	//private SomeServiceProxy theServiceProxy;
	
	//example: Inject an internal service component to leverage an existing enterprise resource
	//@Inject
	//private SomeServiceAdapter theServiceAdapter;
	
	//The following variables correspond to bean properties and are used when this expression is 
	//"interpreted". Typically, they would be named appropriately for the role they play in the 
	//expressions' interpret algorithm (think "customerId", "discountThreshold", "productSKU", 
	//"expirationDate", etc.). Here for demo purposes, they illustrate the use of various data 
	//types as input parms. 
	private String theString;
	private Date theDate;
	private BigDecimal theNumber;
	
	//for demo purposes only  
	private double mockResult;

	public DemoExpression() {
		super();
	}
	
	//Expression bean property values may be assigned dynamically when a formula is executed or statically when the bean
	//is instantiated from the Spring ApplicationContext defined by FormulaExpressionBeans.xml. In either
	//case, Spring injection makes it happen.  
	
	//Dynamic, user-provided property values are assigned using the parameter mappings defined in the <ContextParameterMap>
	//element of the XML formula definitions found in main/resources/formula_defs. This set of <ParameterMapping>'s is 
	//known as the formulaContext. The <property> injections for the <bean> invoke the "setContext**()" property setters. 
	//For properties that require a binding to the formulaContext, the "value" attribute of the <property> is the name of
	//a formulaContext parameter, and the property setter will invoke the parent configProperty() method to create the 
	//binding. This binding resolves to a real value when the interpret() method is invoked. 
	
	//Static bean <property> values are treated normally and assigned directly to private variables. These properties are
	//typically used for configuration. A database lookup bean for example, might be statically configured with table and 
	//column names or enumerated db constants to facilitate a specific type of lookup.  
	
	//Be sure to follow the naming conventions used in these "setters" and calls to the parent configProperty() method
	//when creating new bean classes that extend TerminalExpression.
    
	public void setContextTheString(String contextParmName) {
        this.configProperty("theString", contextParmName);  
    }

    private void setTheStringImpl(Object value) {
      this.theString = (String) value;
    }

	public void setContextTheDate(String contextParmName) {
        this.configProperty("theDate", contextParmName);  
    }

    private void setTheDateImpl(Object value) {
      this.theDate = (Date) value;
    }

	public void setContextTheNumber(String contextParmName) {
		this.configProperty("theNumber", contextParmName);
	}

	private void setTheNumberImpl(Object value) {
		this.theNumber = (BigDecimal) value;
	}

	//For demo only
	public void setMockResult(double contextParm) {
		this.mockResult = contextParm;
	}

	//The formula execution engine invokes the interpret() method to obtain the value of an expression, it passes
	//in the formulaContext for the current formula execution instance.

	@Override
	public Object interpret(FormulaContext formulaContext) throws FormulaException {
		//The super.interpret() method uses the previously established property-to-inputParameter bindings to 
		//invoke the private "set**Impl()" property setters, making the user-provided input values available 
		//to the interpret algorithm. 
		super.interpret(formulaContext);
		
		//Now the value generating algorithm for this expression is invoked, whether 
		//that be a db lookup, a service call, an algorithm or some type of combined workflow. 
		//Regardless of the implementation, the end result is a numerical value.
		
		//All implementations of the interpret() method must call the parent class processResult()
		//method, passing the formulaContext and calculated value. processResult() will apply whatever
		//precision or rounding specified by the formula definition is necessary. 
		//If the formula definition for this expression specifies a "resultName" attribute, 
		//processResult() will also add the final value to the formulaContext tagged with the
		//resultName for future reference. 
		
		//for demo purposes, generate a random "interpreted" value
		//return processResult(formulaContext, ThreadLocalRandom.current().nextDouble(19.34, 382.63));
		
		//for demo purposes, return the statically injected mock result
		return processResult(formulaContext, mockResult);
	}

	@Override
	public String getFormulaName() {
        try {
            return super.getFormulaName().toString();
        } catch (NullPointerException npe) {
            return "Mock DBLkup-ServiceReq";
        }
    }
}
