package org.spinmnt.lasore.expression.util;

import org.spinmnt.lasore.expression.FormulaContext;

import org.spinmnt.lasore.util.DataTypeUtil;
import org.spinmnt.lasore.util.FormulaException;

import java.util.HashMap;

public class ConstantValueParameter extends ContextParameterImpl {
  private Object constValue;
  private boolean isValidated;
  
  public ConstantValueParameter(String parameterName, String dataType, Object constValue) throws FormulaException {
    super(parameterName, dataType);
    this.constValue = constValue;
    isValidated = false;
  }
  
  public void reset(HashMap<String, Object> formulaInput, FormulaContext parentContext) throws FormulaException {
    if (constValue == null) {
      throwMissingInput();  
    }
    if (!isValidated) {
      try {
        constValue = DataTypeUtil.parseValue(constValue, dataType);
        isValidated = true;
      } catch (Exception e) {
        throwInvalidInput(this.constValue);  
      }
    }
  }

  public Object getValue() {
    return constValue;
  }

  public void setConstantValue(Object constValue) {
    this.constValue = constValue;
  }
}
