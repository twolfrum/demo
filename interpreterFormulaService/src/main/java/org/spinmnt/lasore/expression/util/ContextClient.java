package org.spinmnt.lasore.expression.util;

import org.spinmnt.lasore.expression.FormulaContext;
import org.spinmnt.lasore.util.FormulaException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface ContextClient {
  public void applyContext(FormulaContext formulaContext) throws FormulaException;

  public void configProperty(String propertyName, String contextParmName) throws FormulaException;

  public void configProperty(String propertyName,
                             Map<String, String> contextParmMap) throws FormulaException;
  public void configProperty(String propertyName,
                             List<String> contextParmList) throws FormulaException;
}
