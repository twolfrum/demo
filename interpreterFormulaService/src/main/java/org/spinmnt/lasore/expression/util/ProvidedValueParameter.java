package org.spinmnt.lasore.expression.util;

import org.spinmnt.lasore.expression.FormulaContext;

import org.spinmnt.lasore.util.DataTypeUtil;
import org.spinmnt.lasore.util.FormulaException;

import java.util.HashMap;

public class ProvidedValueParameter extends ContextParameterImpl {
  private Object parmValue;
  private String inputValueName;
  private String defaultValue;
  
  public ProvidedValueParameter(String parameterName, String dataType, String inputValueName, String defaultValue) throws FormulaException {
    super(parameterName, dataType);
    this.inputValueName = inputValueName;
    this.defaultValue = defaultValue;
  }
 
  @Override 
  public Object getValue() {
    return parmValue;  
  }
  
  @Override
  public void reset(HashMap<String, Object> formulaInput, FormulaContext parentContext) throws FormulaException {
    if (!resetRequired(parentContext)) {
      return;  
    }

    Object myValue = formulaInput.get(this.inputValueName);
    if (myValue == null || myValue.toString().isEmpty()) {
      myValue = defaultValue;
    }
    if (myValue == null) {
      throwMissingInput(this.inputValueName);  
    }
    try {
      parmValue = DataTypeUtil.parseValue(myValue, dataType);    
    } catch (Exception e) {
      throwInvalidInput(this.inputValueName, myValue);
    }
  }

  public void setInputValueName(String inputValueName) {
    this.inputValueName = inputValueName;
  }

  public String getInputValueName() {
    return inputValueName;
  }

  public void setDefaultValue(String defaultValue) {
    this.defaultValue = defaultValue;
  }

  public String getDefaultValue() {
    return defaultValue;
  }
}
