package org.spinmnt.lasore.expression;

import org.spinmnt.lasore.util.FormulaException;

import java.util.ArrayList;

public class Condition extends SwitchCondition {
  private ArrayList<SwitchCondition> myConditions;
  
  public Condition(String conjunctionName) {
    super(conjunctionName);
    myConditions = new ArrayList<SwitchCondition>();
  }

  public boolean evaluate(FormulaContext formulaContext) throws FormulaException {
    boolean myState = true;
    
    if (!myConditions.isEmpty()) {
      myState = myConditions.get(0).evaluate(formulaContext);  
      for (int i=1; i < myConditions.size(); i++) {
        Conjunction conjunct = myConditions.get(i).getConjunction(); 
        if (conjunct == null) {
          throw new FormulaException("Conjunction missing from formula definition - switch condition cannot be evaluated");  
        }
        boolean state =  myConditions.get(i).evaluate(formulaContext); 
        switch (conjunct) {
          case And:
            myState = (myState && state);
            break;
          case Or:
            myState = (myState || state);
            break;
          case AndNot:
            myState = (myState && !state);
            break;
          case OrNot:
            myState = (myState || !state);
            break;
        }
        if (!myState) {
          break;  
        }
      }
    }
    return myState;
  }

  public void addCondition (SwitchCondition theCondition) {
    myConditions.add(theCondition);
  }
}
