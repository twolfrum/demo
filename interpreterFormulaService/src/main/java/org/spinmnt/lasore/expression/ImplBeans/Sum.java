package org.spinmnt.lasore.expression.ImplBeans;

import org.spinmnt.lasore.expression.FormulaContext;
import org.spinmnt.lasore.expression.TerminalExpression;

import org.spinmnt.lasore.expression.util.ContextParameter;
import org.spinmnt.lasore.util.DataTypeUtil;
import org.spinmnt.lasore.util.FormulaException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Sum extends TerminalExpression implements SumContextClient {
  private List<Object> sumValueList;
  private String[] contextParmNameList;

  public Sum() {
  }

  public Object interpret(FormulaContext formulaContext) throws FormulaException {
    super.interpret(formulaContext);

    double result = 0;
    
    try {
      for (Object sumValue : sumValueList) {
        result += Double.parseDouble(sumValue.toString());
      }
    } catch (Exception e) {
      String msg = "Addition error caused by " + e.getMessage();
      if (getResultName() != null) {
        msg = msg + " while calculating '" + getResultName() + "'";    
      }
      throw new FormulaException(msg);  
    }
        
    return processResult(formulaContext, result);
  }

  public void setContextSummationValues(List<String> contextParmNameList) {
    this.configProperty("summationValues", contextParmNameList);
    this.contextParmNameList = contextParmNameList.toArray(new String[0]);
  }
  
  private void setSummationValuesImpl(List<Object> sumValueList) {
    this.sumValueList = sumValueList;
  }
  
  @Override
  public String getFormulaName() {
    try {
      return super.getFormulaName().toString();
    } catch (NullPointerException npe) {
      return "Sum" + Arrays.toString(this.contextParmNameList);
    }
  }
}
