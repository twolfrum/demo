package org.spinmnt.lasore.expression.ImplBeans;

import org.spinmnt.lasore.expression.FormulaContext;
import org.spinmnt.lasore.expression.TerminalExpression;
import org.spinmnt.lasore.expression.util.ContextParameter;
import org.spinmnt.lasore.util.DataTypeUtil;
import org.spinmnt.lasore.util.FormulaException;

public class Echo extends TerminalExpression implements EchoContextClient{
  private double value; 
  private String contextParmName;
  private boolean useExpressionPrecision;

  public Echo() {
      this.useExpressionPrecision = false;
  }

  public Object interpret(FormulaContext formulaContext) throws FormulaException {
    super.interpret(formulaContext);
    ContextParameter cp = formulaContext.getContextParameter(contextParmName);
    DataTypeUtil.TypeDef td = DataTypeUtil.parseType(cp.getDataType());
    // if expression level precision has not been defined, default to context parm precision 
    if (!this.useExpressionPrecision) {
        this.setResultPrecision(td.getPrecision());
    }
    return processResult(formulaContext, value);
  }

  public void setContextValue (String contextParmName) {
    this.contextParmName = contextParmName;
    this.setFormulaName(contextParmName);
    this.configProperty("value", contextParmName);  
  }

  private void setValueImpl(Object value) {
    this.value = Double.parseDouble(value.toString());
  }
  
  public void setResultPrecision(int resultPrecision) {
    super.setResultPrecision(resultPrecision);
    //capture the fact that precision has been defined at the expression level
    this.useExpressionPrecision = true;
  }
}
