package org.spinmnt.lasore.expression;

import org.spinmnt.lasore.util.FormulaException;

import java.util.HashMap;

public interface Expression {
  public Object interpret(FormulaContext formulaContext) throws FormulaException;
  public String getFormulaName();
  public String getResultName();
  public int getResultPrecision();
  public boolean getRoundEven();
  public boolean getRoundDown();
  public void setFormulaName(String formulaName);
  public void setResultName(String resultName);
  public void setResultPrecision(int precision);
  public void setRoundEven(boolean roundEven);
  public void setRoundDown(boolean roundDown);
  public String getDescription();
  //public void configProperty(String propertyName, String contextParmName);
  //public void configProperty(String propertyName, HashMap<String, String> contextParmMap);
}
