package org.spinmnt.lasore.expression.util;

import org.spinmnt.lasore.expression.FormulaContext;
import org.spinmnt.lasore.util.FormulaException;

import java.util.HashMap;

public interface ContextParameter {
  public void reset(HashMap<String, Object> formulaInput,
                    FormulaContext parentContext) throws FormulaException;

  public Object getValue();

  public void setParameterName(String parameterName);

  public String getParameterName();

  public void setDataType(String dataType);

  public String getDataType();
}
