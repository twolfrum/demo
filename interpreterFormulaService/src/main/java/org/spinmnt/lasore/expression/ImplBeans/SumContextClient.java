package org.spinmnt.lasore.expression.ImplBeans;

import java.util.HashMap;
import java.util.List;

public interface SumContextClient {
  public void setContextSummationValues(List<String> contextParmNameList);
}
