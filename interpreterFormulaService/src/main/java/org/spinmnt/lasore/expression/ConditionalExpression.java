package org.spinmnt.lasore.expression;

import org.spinmnt.lasore.util.FormulaException;

public class ConditionalExpression implements Expression {
  private SwitchCondition myCondition;
  private Expression myExpression;
  
  public ConditionalExpression() {
    super();
  }

  public Object interpret(FormulaContext formulaContext) throws FormulaException {
    if (myCondition.evaluate(formulaContext)) {
      return myExpression.interpret(formulaContext);
    } else {
      return null;
    }
  }

  public String getFormulaName() {
    return myExpression.getFormulaName();
  }

  public String getResultName() {
    return  myExpression.getResultName();
  }

  public int getResultPrecision() {
    return  myExpression.getResultPrecision();
  }

  public boolean getRoundEven() {
    return  myExpression.getRoundEven();
  }

  public boolean getRoundDown() {
    return  myExpression.getRoundDown();
  }

  public void setFormulaName(String formulaName) {
  }

  public void setResultName(String resultName) {
  }

  public void setResultPrecision(int precision) {
  }
  
  public void setRoundEven(boolean roundEven) {
  }

  public void setRoundDown(boolean roundDown) {
  }

  public String getDescription() {
    return null;
  }

  public void setCondition(SwitchCondition condition) {
    this.myCondition = condition;
  }

  public SwitchCondition getCondition() {
    return myCondition;
  }

  public void setExpression(Expression expression) {
    this.myExpression = expression;
  }

  public Expression getExpression() {
    return myExpression;
  }
}
