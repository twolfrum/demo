package org.spinmnt.lasore.expression;

import org.spinmnt.lasore.expression.util.ContextClient;
import org.spinmnt.lasore.expression.util.ContextClientImpl;
import org.spinmnt.lasore.util.DataTypeUtil;
import org.spinmnt.lasore.util.FormulaException;

import java.math.BigDecimal;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConditionTest extends SwitchCondition implements ContextClient {
  private String dataType;
  private DataTypeUtil.TypeDef typeDef;
  private Operator operator;
  private Object leftOperand; 
  private Object rightOperand;
  private ContextClientImpl contextDelegate;

  public enum Operator {
    EQ, NE, GT, LT, GTE, LTE;
    
    public static Operator resolve (String operName) {
      Operator resolved = null;
      for (Operator oper : Operator.values()) {
        if (oper.name().equals(operName)) {
          resolved = oper;
          break;
        }
      }
      return resolved;
    }
  }
  
  public ConditionTest(String conjunctionName) {
    super(conjunctionName);
    contextDelegate = new ContextClientImpl(this); 
  }

  public boolean evaluate(FormulaContext formulaContext) throws FormulaException {
    applyContext(formulaContext);
    int compare = 0;
    switch (this.typeDef.getDataType()) {
      case STRING:
        compare = ((String)this.leftOperand).compareTo((String)this.rightOperand);
        break;
      case DATE:
        compare = ((Date)this.leftOperand).compareTo((Date)this.rightOperand);
        break;
      case NUMBER:
        compare = ((BigDecimal)this.leftOperand).compareTo((BigDecimal)this.rightOperand);
        break;
    }
    
    if (compare == 0) { // left = right
      switch (this.operator) {
        case EQ:
        case GTE:
        case LTE:
          return true;
        default:
          return false;
      }
    } else if (compare > 0) { // left > right
      switch (this.operator) {
        case GT:
        case GTE:
        case NE:
          return true;
        default:
          return false;
        } 
    } else if (compare < 0) { // left < right
      switch (this.operator) {
        case LT:
        case LTE:
        case NE:
          return true;
        default:
          return false;
        }
    }
    return false;
  }

  public void setDataType(String dataType) {
    this.dataType = dataType;
    this.typeDef = DataTypeUtil.parseType(dataType); 
  }

  public String getDataType() {
    return dataType;
  }
  
  public void setOperator(String operName) {
    this.operator = Operator.resolve(operName);  
  }

  public void setOperator(ConditionTest.Operator operator) {
    this.operator = operator;
  }

  public ConditionTest.Operator getOperator() {
    return operator;
  }

  public void setContextLeftOperand(String contextParmName) {
    this.configProperty("leftOperand", contextParmName);  
  }
  
  private void setLeftOperandImpl(Object operand) {
    this.leftOperand = operand;
  }

  public void setContextRightOperand(String contextParmName) {
    this.configProperty("rightOperand", contextParmName);  
  }
  
  private void setRightOperandImpl(Object operand) {
    this.rightOperand = operand;
  }
  
  public void applyContext(FormulaContext formulaContext) throws FormulaException {
    contextDelegate.applyContext(formulaContext);
  }

  public void configProperty(String propertyName,
                             Map<String, String> contextParmMap) {
    contextDelegate.configProperty(propertyName, contextParmMap);
  }

  public void configProperty(String propertyName,
                             List<String> contextParmList) {
    contextDelegate.configProperty(propertyName, contextParmList);
  }

  public void configProperty(String propertyName, String contextParmName) {
    contextDelegate.configProperty(propertyName, contextParmName);
  }

}
