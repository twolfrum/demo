package org.spinmnt.lasore.expression.ImplBeans;

import org.spinmnt.lasore.expression.FormulaContext;
import org.spinmnt.lasore.expression.NonTerminalExpression;
import org.spinmnt.lasore.util.FormulaException;

public class Max extends NonTerminalExpression {
  public Max() {
  }

  public Object interpret(FormulaContext formulaContext) throws FormulaException {
    super.interpret(formulaContext);

    double result = 0;
    double left = (Double)this.getLeftOperand().interpret(formulaContext);
    double right = (Double)this.getRightOperand().interpret(formulaContext);
    
    result = Math.max(left, right);    
    return processResult(formulaContext, result);
  }
  
  @Override
  public String getFormulaName() {
    try {
      return super.getFormulaName().toString();
    } catch (NullPointerException npe) {
      return "max";
    }
  }
}
