package org.spinmnt.lasore.expression.util;


import org.spinmnt.lasore.expression.FormulaContext;

import org.spinmnt.lasore.util.FormulaException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ContextClientImpl implements ContextClient {
  private HashMap<String, ExpressionPropertySetter> propertySetters;
  private Object expBean; 
  
  //Default constructor - invoke when extending this class
  public ContextClientImpl() {
    super();
    init();
    this.expBean = this;
  }
  
  //Alternate constructor to be used when this class is used as a delegate
  public ContextClientImpl (Object expBean) {
    super();
    init();
    this.expBean = expBean;
  }
  
  private void init() {
    this.propertySetters = new HashMap<String, ExpressionPropertySetter>();
  }
  
  public void applyContext (FormulaContext formulaContext)  throws FormulaException {
    for (String propName : propertySetters.keySet()) {
      //contextParameterMap key = name of ContextParameter
      //contextParameterMap value = ContextParameter object
      propertySetters.get(propName).execute(formulaContext);  
    }
  }

  public void configProperty(String propertyName, String contextParmName) {
    ValuePropertySetter vps = new ValuePropertySetter(propertyName, this.expBean, contextParmName);
    //property name may have been transformed by property setter constructor
    propertySetters.put(vps.getPropertyName(), vps);
  }
  
  public void configProperty(String propertyName, Map<String, String> contextParmMap) {
    MapPropertySetter mps = new MapPropertySetter(propertyName, this.expBean, contextParmMap);
    //property name may have been transformed by property setter constructor
    propertySetters.put(mps.getPropertyName(), mps);
  }
  
  public void configProperty(String propertyName, List<String> contextParmList) {
    ListPropertySetter lps = new ListPropertySetter(propertyName, this.expBean, contextParmList);
    //property name may have been transformed by property setter constructor
    propertySetters.put(lps.getPropertyName(), lps);
  }
  
  private abstract class ExpressionPropertySetter {
    private String propertyName;
    private Object expBean;

    private ExpressionPropertySetter(String propertyName, Object expBean) {
      super();
      //if the property setter is being called directly from the XMLFormulaBuilder class, the 
      //property name will be prefixed with "context" - remove the prefix
      if (propertyName.startsWith("context")) {
        propertyName = propertyName.replaceFirst("context", "");
        propertyName = Character.toLowerCase(propertyName.charAt(0)) + propertyName.substring(1);
      }
      this.propertyName = propertyName;
      this.expBean = expBean;
    }

    abstract void execute(FormulaContext formulaContext) throws FormulaException;
      //if property type is single value:
      //setProperty(expBean, Object, value);  
      //if property type is map:
      //setProperty(expBean, HashMap, value);

    protected final void setProperty(Class propertyClass, Object value) throws FormulaException {
      ExpressionPropertyUtil.set(this.expBean, this.propertyName + "Impl", propertyClass, value);
    }
    
    protected void setPropertyName(String propertyName) {
      this.propertyName = propertyName;
    }

    protected String getPropertyName() {
      return propertyName;
    }

    private void setExpBean(Object expBean) {
      this.expBean = expBean;
    }

    private Object getExpBean() {
      return expBean;
    }
  }
  
  private class ValuePropertySetter extends ExpressionPropertySetter {
    private String contextParmName;

    private ValuePropertySetter(String propertyName, Object expBean, String contextParmName) {
      super(propertyName, expBean);
      this.contextParmName = contextParmName;
    }

    protected void execute(FormulaContext formulaContext)  throws FormulaException {
      try {
        this.setProperty(Object.class, formulaContext.getContextParameter(contextParmName).getValue());
      } catch (NullPointerException npe) {
        throw new FormulaException ("Required context parameter '" + contextParmName + "' not found");   
      }
    }
  }
  
  private class MapPropertySetter extends ExpressionPropertySetter {
    private Map<String, String> propertyMap;
    
    private MapPropertySetter (String propertyName, Object expBean, Map<String, String> propertyMap) {
      //propertyMap key = name for a name-value pair that is being managed by the map-type property identified by propertyName 
      //propertyMap value = name of the Context Parameter that is the source of the value for the name-value pair  
      super(propertyName, expBean);
      this.propertyMap = propertyMap;  
    }
    
    protected void execute(FormulaContext formulaContext)  throws FormulaException {
      Map methMap = new HashMap<String, Object>();
      for (String mapKey : this.propertyMap.keySet()) {
        try {
          methMap.put(mapKey, formulaContext.getContextParameter(propertyMap.get(mapKey)).getValue());
        } catch (NullPointerException npe) {
          throw new FormulaException ("Required context parameter '" + propertyMap.get(mapKey) + "' not found");   
        }
      }
      this.setProperty(Map.class, methMap);
    }
  }
  
  private class ListPropertySetter extends ExpressionPropertySetter {
    private List<String> propertyList;
    
    private ListPropertySetter (String propertyName, Object expBean, List<String> propertyList) {
      //propertyList is a list of Context Parameter names  
      super(propertyName, expBean);
      this.propertyList = propertyList;  
    }
    
    protected void execute(FormulaContext formulaContext)  throws FormulaException {
      List methList = new ArrayList<Object>();
      for (String parmName : this.propertyList) {
        try {
          methList.add(formulaContext.getContextParameter(parmName).getValue());
        } catch (NullPointerException npe) {
          throw new FormulaException ("Required context parameter '" + parmName + "' not found");   
        }
      }
      this.setProperty(List.class, methList);
    }
  }
}
