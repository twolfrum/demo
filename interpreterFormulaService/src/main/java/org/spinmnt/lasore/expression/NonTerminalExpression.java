package org.spinmnt.lasore.expression;

public abstract class NonTerminalExpression extends AbstractExpression {
    private Expression leftOperand;
    private Expression rightOperand;


    public void setLeftOperand(Expression leftOperand) {
        this.leftOperand = leftOperand;
    }

    public Expression getLeftOperand() {
        return leftOperand;
    }

    public void setRightOperand(Expression rightOperand) {
        this.rightOperand = rightOperand;
    }

    public Expression getRightOperand() {
        return rightOperand;
    }
    
    public final String getDescription() {
      StringBuffer buff = new StringBuffer();
      buff.append("(");
      buff.append(getLeftOperand().getDescription());
      buff.append(" ");
      buff.append(getFormulaName());
      buff.append(" ");
      buff.append(getRightOperand().getDescription());
      buff.append(")");
      return buff.toString();
    }
}
