package org.spinmnt.lasore.expression.util;

import org.spinmnt.lasore.expression.FormulaContext;
import org.spinmnt.lasore.util.FormulaException;

import java.util.HashMap;

public class ReferenceParameter implements ContextParameter {
  private String referenceName;
  private ContextParameter reference;
  
  public ReferenceParameter(String referenceName) {
    this.referenceName = referenceName;
  }

  public void reset(HashMap<String, Object> formulaInput,
                    FormulaContext parentContext) throws FormulaException {
    reference = parentContext.getContextParameter(referenceName);
    if (reference == null) {
      throw new FormulaException("Referenced context parameter '" + referenceName + "' not found");  
    }
    reference.reset(formulaInput, parentContext);
  }

  public Object getValue() {
    Object theValue;
    try {
      theValue = reference.getValue();  
    } catch (Exception e) {
      return "$NA$";    
    }
    return theValue;
  }

  public String getDataType() {
    String theType;
    try {
      theType = reference.getDataType();  
    } catch (Exception e) {
      return "$NA$";    
    }
    return theType;
  }

  public String getParameterName() {
    String theName;
    try {
      theName = reference.getParameterName(); 
    } catch (Exception e) {
      return "$NA$";    
    }
    return theName;
  }

  public void setDataType(String dataType) {
    //cannot change data type of a referenced parm
  }

  public void setParameterName(String parameterName) {
    //cannot change name of a referenced parm
  }
}
