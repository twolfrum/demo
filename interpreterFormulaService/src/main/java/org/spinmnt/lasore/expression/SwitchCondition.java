package org.spinmnt.lasore.expression;

import org.spinmnt.lasore.util.FormulaException;

public abstract class SwitchCondition {
  protected SwitchCondition.Conjunction conjunction; 

  public enum Conjunction {And, Or, AndNot, OrNot}

  public abstract boolean evaluate(FormulaContext formulaContext) throws FormulaException;
  
  public SwitchCondition (String conjunctionName) {
    //this constructor is called from XMLFormulaBuilder - conjunction name may be null
    try {
      if (conjunctionName.equals("And")) {
        this.conjunction = Conjunction.And;  
      } else if (conjunctionName.equals("Or")) {
        this.conjunction = Conjunction.Or;  
      } else if (conjunctionName.equals("AndNot")) {
        this.conjunction = Conjunction.AndNot;  
      } else if (conjunctionName.equals("OrNot")) {
        this.conjunction = Conjunction.OrNot;  
      }
    } catch (NullPointerException npe) {}//no big deal
  }

  public void setConjunction(SwitchCondition.Conjunction conjunction) {
    this.conjunction = conjunction;
  }

  public SwitchCondition.Conjunction getConjunction() {
    return this.conjunction;
  }
  
}
