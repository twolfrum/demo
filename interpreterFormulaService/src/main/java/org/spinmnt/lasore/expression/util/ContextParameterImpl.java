package org.spinmnt.lasore.expression.util;

import org.spinmnt.lasore.expression.FormulaContext;

import org.spinmnt.lasore.util.FormulaException;

import java.util.HashMap;

public abstract class ContextParameterImpl implements ContextParameter {
  protected String parameterName;
  protected String dataType;
  private long execRequestId;
  
  public ContextParameterImpl(String parameterName, String dataType) throws FormulaException {
    this.parameterName = parameterName;
    execRequestId = 0;
    try {
      this.dataType = dataType.toString();
    } catch (NullPointerException npe) {
      throw new FormulaException ("dataType attribute missing for context parameter '" + parameterName + "'");  
    }
  }

  public void setParameterName(String parameterName) {
    this.parameterName = parameterName;
  }

  public String getParameterName() {
    return parameterName;
  }
  
  public void setDataType(String dataType) {
    this.dataType = dataType;
  }

  public String getDataType() {
    return dataType;
  }
  
  public abstract Object getValue();
  
  public abstract void reset(HashMap<String, Object> formulaInput, FormulaContext parentContext) throws FormulaException;
  
  protected boolean resetRequired(FormulaContext formulaContext) {
    if (execRequestId == formulaContext.getExecRequestId()) {
      return false;  
    } else {
      execRequestId = formulaContext.getExecRequestId();
      return true;
    }
  }
  
  protected void throwMissingInput(String inputName) throws FormulaException {
    throw new FormulaException("Missing input value for " + inputName);    
  }

  protected void throwMissingInput() throws FormulaException {
    throwMissingInput(this.parameterName);  
  }

  protected void throwInvalidInput(String inputName, Object inputValue) throws FormulaException {
    throw new FormulaException("Input value '" + inputValue.toString() + "' for <" + inputName + "> cannot be interpreted as " + this.dataType);    
  }

  protected void throwInvalidInput(Object inputValue) throws FormulaException {
    throwInvalidInput(this.parameterName, inputValue);  
  }

}
