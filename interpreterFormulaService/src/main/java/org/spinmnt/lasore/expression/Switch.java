package org.spinmnt.lasore.expression;

import org.spinmnt.lasore.util.FormulaException;

import java.util.ArrayList;

public class Switch implements Expression {
  private ArrayList<ConditionalExpression> expressions;
  private Expression myDefault;
  private Expression lastExecuted;
  private String formulaName;
  
  public Switch() {
    super();
    expressions = new ArrayList<ConditionalExpression>();
    myDefault = null;
    lastExecuted = null;
  }

  public Object interpret(FormulaContext formulaContext) throws FormulaException {
    lastExecuted = null;
    for (ConditionalExpression ce : expressions) {
      Object result = ce.interpret(formulaContext);
      if (result != null) {
        lastExecuted = ce.getExpression();
        return result;  
      }
    }
    if (myDefault != null) {
      lastExecuted = myDefault;
      return myDefault.interpret(formulaContext); 
    } else {
      throw new FormulaException("No conditions were satisfied for '" + formulaName + "' and no default calculation or value is specified");
    }
  }
  
  public void setDefault(Expression theDefault) {
    myDefault = theDefault;  
  }

  public void addExpression (ConditionalExpression cExpression) {
    expressions.add(cExpression);      
  }
  
  public String getFormulaName() {
    return this.formulaName;
  }

  public String getResultName() {
    try {
      return lastExecuted.getResultName();
    } catch (Exception e) {
      try {
        return myDefault.getResultName();  
      } catch (Exception ee) {
        return "";  
      }
    }
  }

  public int getResultPrecision() {
    try {
      return lastExecuted.getResultPrecision();
    } catch (Exception e) {
      try {
        return myDefault.getResultPrecision();  
      } catch (Exception ee) {
        return 0;  
      }
    }
  }

  public boolean getRoundEven() {
    try {
      return lastExecuted.getRoundEven();
    } catch (Exception e) {
      try {
        return myDefault.getRoundEven();  
      } catch (Exception ee) {
        return false;  
      }
    }
  }

  public boolean getRoundDown() {
    try {
      return lastExecuted.getRoundDown();
    } catch (Exception e) {
      try {
        return myDefault.getRoundDown();  
      } catch (Exception ee) {
        return false;  
      }
    }
  }

  public String getDescription() {
    try {
      return lastExecuted.getDescription();
    } catch (Exception e) {
      try {
        return myDefault.getDescription();  
      } catch (Exception ee) {
        return "";  
      }
    }
  }

  public void setFormulaName(String formulaName) {
    this.formulaName = formulaName;
  }

  public void setResultName(String resultName) {
  }

  public void setResultPrecision(int precision) {
  }

  public void setRoundEven(boolean roundEven) {
  }

  public void setRoundDown(boolean roundDown) {
  }
}
