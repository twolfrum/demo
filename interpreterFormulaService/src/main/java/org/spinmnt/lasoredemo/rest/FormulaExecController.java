package org.spinmnt.lasoredemo.rest;

import org.springframework.web.bind.annotation.*;
import org.xml.sax.SAXException;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;

import org.spinmnt.lasore.ExecutableFormula;
import org.spinmnt.lasore.XMLFormulaBuilder;
import org.spinmnt.lasore.util.FormulaException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@RestController
@RequestMapping("/formula")
public class FormulaExecController {

	@PostMapping("/exec/{formulaName}")
	public ResponseEntity<Map<String, BigDecimal>> executeFormula(@PathVariable("formulaName") String formulaName,
			@RequestBody HashMap<String, Object> formulaInput, HttpServletResponse response) {

		XMLFormulaBuilder builder = new XMLFormulaBuilder();
		ExecutableFormula formula = null;

		try {
			formula = builder.buildFormulaFromDefinition(formulaName, "NA");
		} catch (JAXBException e) {
			e.printStackTrace();
			return createErrorResponse(response, e.getMessage());
		} catch (SAXException e) {
			e.printStackTrace();
			return createErrorResponse(response, e.getMessage());
		} catch (FormulaException e) {
			e.printStackTrace();
			return createErrorResponse(response, e.getMessage());
		}

		Map<String, BigDecimal> result = null;
		try {
			result = formula.execute(formulaInput, null);
			for (String resultName : result.keySet()) {
				System.out.println(resultName + "=" + result.get(resultName).toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return createErrorResponse(response, e.getMessage());
		}

		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	private ResponseEntity<Map<String, BigDecimal>> createErrorResponse( HttpServletResponse response, String errMsg) {
		ResponseEntity<Map<String, BigDecimal>> errResponseEntity = new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		response.setHeader("Formula-Exec-Error", errMsg);
		return errResponseEntity;
	}
}
