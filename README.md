# LASORE #
## Lightweight Arithmetic Service Oriented Rules Engine ##

A novel use of the OO Interpreter Pattern that leverages Spring Dependency Injection and XML to create an extensible framework for defining and executing portable, rule-influenced arithmetic formulas.

## Overview ##
Based on the Behavioral OO Design Pattern known as "Interpreter", LASORE is an extensible framework for defining and executing arithmetic formulas that can be easily coupled with enterprise resources to produce a use-case specific value. As stated in the time-honored reference "Design Patterns - Elements of Reusable Object-Oriented Software" written by Erich Gamma, Richard Helm, Ralph Johnson and John Vlissedes (aka "Gang of Four"), the intent of the Interpreter pattern is:

    "Given a language, define a representation for its grammar along with an interpreter that uses the representation to interpret sentences in the language."

The "language" of LASORE is primarily a combination numeric and boolean expressions. The "representation for its grammar" is XML based on an XSD schema. And finally, the "interpreter" is a Java program that leverages Spring Dependency Injection to translate elements of the grammar into executable software components.

## Intent ##
The intent of the LASORE framework is to provide a lightweight, customizable platform that can be used across an enterprise to define and implement business rules for arithmetic calculations. For example, consider a business where the fees it charges for its services and products is a function of many variables. These variables may include the customer's order history, attributes of the customer (geo-location, trade association membership, etc.), purchasing contract terms, pre-determined fee schedules or time-sensitive discounts to name a few. Fee calculation requires a workflow using various DB lookups and service-oriented resources to gather all the required numerical data elements.      

Of course, there are already enterprise-level, business-rule driven workflow orchestration platforms available to address the requirement described above. Many of these platforms tend to be "heavyweight" implementations, loaded with features that may never used, involve significant configuration, and require the purchase of a license key.      

The LASORE framework aims to be a "lightweight", yet adequately robust alternative to the heavyweight solution.  It can for example, be stood up as a service itself (as demonstrated in this repo), embedded as a component in other services, used as a library within an application, or even added directly to the code-base of a desktop application.

## About the Repo ##
This repo contains the source code for the LASORE framework wrapped as a simple, single resource REST service named "interpreterFormulaService". This service is an example of a **framework implementation instance**. In addition to the core framework, it also includes examples of formula definitions that demonstrate the fundamental aspects of the formula grammar, and the associated Spring Application Context required by the engine for formula execution. Here is an overview of interpreterFormulaService folder/package structure:

+ Framework Core Packages - Foundational classes required by any framework implementation instance
  + **org.spinmnt.lasore** - Root package of LASORE framework
  + **org.spinmnt.lasore.definition** - JAXB generated classes defined by the "org/spinmnt/lasore/formula" schema 
  + **org.spinmnt.lasore.expression** - Classes comprising the executable implementation of the formula grammar
  + **org.spinmnt.lasore.expression.util** - Formula execution support classes; Manage formula execution input and results   
  + **org.spinmnt.lasore.util** - Common utilities used by all classes in the framework

+ Framework Implementation - Classes and resources defining the framework implementation instance
  + **src/main/resources/formula_defs** - Contains nonsensical .xml formula definitions for demonstrating framework functionality and the FormulaDefinition.xsd schema
  + **src/main/resources/FormulaExpressionBeans.xml** - Spring Application Context;  
  + **org.spinmnt.lasore.expression.ImplBeans** - Implementation classes for all &lt;bean&gt; definitions in the Spring Application Context

+ REST Service
  + **org.spinmnt.lasoredemo.rest** - REST service for demonstrating the ETE functioning of the framework implementation

+ Testing
  + **src/main/test** - Functional test that executes all the formulas defined in src/main/resources/formula_defs with various combinations of inputs to demonstrate the features of the framework by mocking the REST service
  + **soap-ui** - Located in the root folder of the repo, contains a soap-ui project for executing several of same tests defined above on a locally deployed instance of the interpreterFormulaService.   
  
## Framework Architecture ##
The architecture of the LASORE framework has 3 fundamental components described here:

* Formula Definition Repository

    Any implementation of the LASORE framework includes a repository of formula definitions based on the FormulaDefinition.xsd schema (namespace: "org/spinmnt/lasore/formula"). This is a runtime-accessible collection of .xml documents. How this repository implemented is at the discretion of the framework implementor. In the interpreterFormulaService project, the formula repository is the following folder:

        src/main/Resources/formula_defs

    Alternatively, the formula definitions could be stored in a db or some type of document repository.

* Spring Application Context

    The Spring Application Context configures the executable components used by formulas, making the framework both customizable and extensible. All the expressions defined in a formula definition, whether Terminal or Non-Terminal, are coupled to a specific Spring Application Context &lt;bean&gt; instance via its' "id" attribute. This coupling between formula definitions and Spring bean definitions is fundamental to how the whole thing works. In the interpreterFormulaService project, the application context is defined by:

        src/main/Resources/FormulaExpressionBeans.xml
    
     The LASORE framework requires that any bean defined in the application context that functions as a formula expression **MUST** be an instance of a class that implements either the TerminalExpression or NonTerminalExpression interface defined in the following package:

        org.spinmnt.lasore.expression

     In the interpreterFormulaService project, the implementation classes for all the application context beans are found in the following package:

        org.spinmnt.lasore.expression.ImplBeans

     Given the numerical-expression nature of Formula Definitions, the typical Non-Terminal expression represents a standard arithmetic operator ("add", "subtract", "multiply", etc.). Thus the terms "Non-Terminal Expression" and "operator" mean essentially the same thing. The standard arithmetic operators are included in FormulaExpressionBeans.xml. The framework can be extended by creating custom operators, such as the "Discount" operator defined in FormulaExpressionBeans.xml.         
     
     Almost all Terminal expressions will be some type of custom implementation defined by the framework implementor. The role of Terminal expressions is to accept any number inputs and return a value. Typically, this value is obtained by leveraging the inputs along with any number of enterprise resources (DB's, internal/3d Party services, enterprise utilities, etc.). A Terminal expressison class could be a very lightweight service request proxy or it could be a "heavy" enterprise resource in and of itself.

     There are two Non-Terminal "utility" expression classes included in the interpreterFormulaService project that could be considered standard components of the LASORE framework. The "echo" expression is specifically used to accept a single numerical value as input and return it when "interpreted". The "sum" expression functions somewhat like the algebraic "Summation" operator - it accepts a variable list of numeric values and returns their sum.                
  
* Formula Execution Engine
  + Conditional Logic    
    The LASORE framework extends the Interpreter an additional expression for interpreting conditional logic. This greatly increases the flexibility of the formula grammar to support complicated business rules.
    The &lt;Switch&gt; expression can be used just like a Terminal/NonTerminal expression anywhere within a formula. It functions essentially like the "switch" statement used in Javascript. A &lt;Switch&gt; encloses any number of boolean conditions associated with a Terminal or NonTerminal expression, and must include a &lt;Default&gt; Terminal/NonTerminal expression. Like all expressions, a &lt;Switch&gt; is "interpreted" to return a value. That value comes from the expression associated with the first boolean condition that is true, or the value of the &lt;Default&gt; expression if none are true.      

  + Formula Execution    
    The actual execution of a formula is managed by 2 classes: org.spinmnt.lasore.XMLFormulaBuilder.java and org.spinmnt.lasore.ExecutableFormula.java.

    Before a formula can be executed, it's definition must first be converted to what the Gang Of Four calls an *abstract syntax tree*. This is the function of the XMLFormulaBuilder class. Using the JAXB generated classes in the org.spinmnt.lasore.definition package and the formula expresssion implementation classes in org.spinmnt.lasore.expression.ImplBeans, it converts an .xml formula definition file into a hierarchical tree of instantiated objects where the root node of the tree corresponds to the &lt;RootExpression&gt; of the formula. This object tree is wrapped in an instance of the ExecutableFormula class. The ExecutableFormula object accepts the user-provided inputs to the formula, executes it and returns the result.        

### Embedded Documentation ###
More detailed documentation on the intricacies of .xml formula definitions and how to implement expression beans can be found in the corresponding examples used by the interpreterFormulaService project. The comments found in org.spinmnt.lasore.expression.ImplBeans.DemoExpression.java in particular describe what a framework implementor needs to know about creating a Terminal Expression class.

FormulaOne.xml, FormulaTwo.xml and FormulaThree.xml are an intro to basic formula definition concepts. FormulaRootSwitch.xml explains the use of the &lt;Switch&gt; expression while FormulaCompoundBool.xml dives a little a deeper by using compound boolean expressions. Lastly, FormulaX.xml combines multiple formula definition features to demonstrate how a reasonably complex formula might look. 

### Building and Running interpreterFormulaService ###
interpreterFormulaService is a pretty straightforward maven project. mvn clean install writes a new .war file to the /target folder which can be deployed as is to a Tomcat server. FormulaExecControllerMockTest.java can be executed with mvn test or directly from within your IDE. If doing the latter, your IDE needs to be configured for a JAXB Provider.

If you have the service running on a server, the soap-ui project is setup to duplicate a few of the  formula executions used by FormulaExecControllerMockTest. A good way to validate your understanding of the framework would be to play around with the formula input values in soap-ui and try to predict the result that will be returned. Or maybe try creating your own formula definition and executing it.        

### Looking Ahead ###
A possible next step is to build a formula authoring tool targeted for BA's. The basic concept is for a UI that uses "palettes" of formula expressions developed by their development team. These palettes would essentially be .xml instances of the Spring Application Context schema, with the expression beans further decorated with &lt;meta&gt; properties to be used by the authoring tool. 
The user drags and drops expressions from their pallette as they build a graphical representation of the formula along with a list of all the required input parameters. 

The vision is that instead of BA's writing specs for business rules that then need to be implemented by writing, testing and deploying code, they define the business rules themselves as schema validated .xml formula definitions. No coding required!    

### Comments or Questions? ###
Send a message to twolfrum@gmail.com